---
sidebar_position: 3
---

# User Profile API

### A. Get My Profile
This API is to show your user information

|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile | X-Token: {{YOUR_TOKEN}} | |

Example output response
```json
{
    "message": "Get my profile successfully!",
    "statusCode": 200,
    "success": true,
    "data": {
        "created_at": 1666643963244,
        "modified_at": 1666643963244,
        "id": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "username": "aalfiann",
        "fullname": null,
        "email": "aalfiann@gmail.com",
        "role": "admin",
        "method": "internal",
        "service": "internal",
        "data": null,
        "status": 1,
        "created_at_month": "October",
        "created_at_year": 2022,
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1"
    }
}
```


### B. Update My Profile
This API is to modify your user information


|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile/update | X-Token: {{YOUR_TOKEN}}, Content-Type: application/json | {"fullname": "M ABD AZIZ ALFIAN", "email": "aalfiann@gmail.com", "data": {"address": "Jakarta Timur DKI Jakarta", "about_me": "Just ordinary developer"}} |

Example output response
```json
{
    "message": "Your profile successfully updated!",
    "statusCode": 200,
    "success": true,
    "data": [
        1
    ]
}
```

**Note:**
- Field `data` is an object type, you can set whatever information on your data user.


### C. Deactivate My Profile
This API is to deactivate or delete your account.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/my-profile/suspend | X-Token: {{YOUR_TOKEN}}, Content-Type: application/json | {"username": "aalfiann"} |

Example output response
```json
{
    "message": "Your profile successfully suspended!",
    "statusCode": 200,
    "success": true,
    "data": [
        1
    ]
}
```

**Note:**
- This action will make your profile deactivated and permanently suspended.
- Nobody can't find you anymore.
- Your username will not available anymore (nobody can't use your username).


### D. Get Another User Profile
This API is to show another user information.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| GET | http://localhost:3000/api/user/profile/{{username}} | | |

Example output response
```json
{
    "message": "Get user profile successfully!",
    "statusCode": 200,
    "success": true,
    "data": {
        "created_at": 1666643963244,
        "modified_at": 1672776525314,
        "id": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "username": "aalfiann",
        "fullname": "M ABD AZIZ ALFIAN",
        "method": "internal",
        "service": "internal",
        "data": {
            "address": "Jakarta Timur DKI Jakarta",
            "about_me": "Just ordinary developer"
        },
        "status": 1,
        "created_at_month": "October",
        "created_at_year": 2022,
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1"
    }
}
```

:::secondary
- Replace `{{username}}` with the username of other user.
- This is public API, so you can request it without X-Token header. 
:::

:::tip API via Postman
There is 50 API used in OpenSSO. Many of them are used for internal system. You can learn it or make an experiment directly using `Postman`. Just import the file `postman_collection.json` (located at root directory) to your Postman Application.
:::