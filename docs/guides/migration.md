---
sidebar_position: 5
---

# Migration
This guide will explain how to migration your existing user into OpenSSO.

:::secondary
- This guide is for completely import all your existing users into OpenSSO.
- Make sure your OpenSSO version is `1.5.0 or higher`.
:::

## Migration Step

### 1. Make Sure Integration Successfully Done.
Before doing migration, make sure the integration already successful. Please read about integration [here](/docs/guides/integration).

### 2. Import Existing User Data
There is two ways to import your existing user data into OpenSSO.

**A. Import Directly into Database**  
You can use using any Database tools. i.e `PHPMyAdmin`, `SQL Workbench`, `DBeaver CE`, `Navicat`, etc.

1. Export your existing user from database into csv/xlsx.
2. You will required to edit the csv/xlsx following the structure OpenSSO users table.
3. Import it using tools. i.e `PHPMyAdmin`, `SQL Workbench`, `DBeaver CE`, `Navicat`, etc.
4. Done.

**B. Import using API**  
Or You can create your own script to import your existing user using our API.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/data/import | Content-Type: application/json, X-Token: {{YOUR-X-TOKEN}} | **bodyJson** |

**Note:**  
- `YOUR-X-TOKEN` is a generated token. You can get it from [here](/docs/guides/api/x-token). 
- `bodyJson` is a json string:

```json
{
    // "id": "", // optional, you're able to inject the old user id
	"username":"yourusername",  // required, loose validation, but you should follow alphanumeric, dot, underscore, plus, lowercase, minLength 3 or maxLength 64
	"email":"youremail@gmail.com"  // required, loose validation, lowercase email format
	// "password":"123456",  // optional, you can let your user to use forgot password, minLength 6 & maxLength 20
    // "name": "Tester 22",  // optional, you can set their fullname
    // "data": { // optional, field data is dynamic, you can input anything / whatever information at here
    //     "address": "",
    //     "whatever": ""
    // },
    // "gravatar": "", // optional, this is the url / link image for user profile picture
    // "hash_value": "",  // optional, you can use this if using same algorithm bcrypt with rounds 10
    // "status": 1,  // optional, you are able to import your suspended user, 1 is active, 0 is suspended
    // "created_at": 1234567890123,  // optional, you can't miss their registered date (unix date)
    // "modified_at": 1718338988865  // optional, you can't miss their last modified date (unix date)
}
```

:::tip
The validation on **username** and **email** is: 

- min length: 3
- max length: 64
- must be lowercase
- alphabet
- or with numeric
- or with underscore
- or with dot (.)
- or with plus (+)


You can use this Regex to validate:
- **username** : ```/^[a-z0-9_.+]{3,64}$/```
- **email** : ```/^(?=.{1,64}@)[a-z0-9._%+-]+@([a-z0-9-]+\.)+[a-z]{2,}$/```

FYI:  
Whatever the name part on email, it will support to be used as default username for oauth.
:::


### 3. Done
Now all your user already imported into OpenSSO. They will can not using their old password, so just let them using `Forgot Password` to get their account back.

:::secondary
- Never tell or announce to anyone the default password, just tell them to use forgot password.
- Forgot password is required a verified email address. Any users with fake email, they will lose their account.
:::