---
sidebar_position: 4
---

# Sign-in With Gitlab
OpenSSO support Sign in with Gitlab. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  // ---
  gitlab: {
    enable: false,
    client_id: 'XXXXXXXXXXXX',
    client_secret: 'XXXXXXXXXXXX'
  },
}
// ---
```

**Description:**
- **gitlab.enable**  if set to true, then this feature will be activated.
- **gitlab.client_id** is the client id generated from Gitlab.
- **gitlab.client_secret** is the client secret generated Gitlab.


### How to Get **client_id** and **client_secret**
1. **Go to GitLab**: Log in to your GitLab account and navigate to your account settings.
2. **Create a New Application**:  
  - Under "Applications," click "New application."
  - Fill in the required details:
    - **Name**: Your application name.
    - **Redirect URI**: You need to input 2 url callback here, see picture below.
    - **Scopes**: read_user.  
    ![](/img/content/blog/gitlab-oauth-setting.png)
3. Click **Save Application**.
4. Done, now you already have the `client_id` and `client_secret`.

:::info
Gitlab Oauth is required an URL with SSL.
:::

