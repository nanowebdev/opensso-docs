---
sidebar_position: 1
---

# Sign-in With Facebook
OpenSSO support Sign in with Facebook. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  // ---
  facebook: {
    enable: false,
    appId: 'XXXXXXXXXXXX',
    appSecret: 'XXXXXXXXXXXX'
  },
}
// ---
```

**Description:**
- **facebook.enable**  if set to true, then this feature will be activated.
- **facebook.appId** is the app id generated from Facebook.
- **facebook.appSecret** is the app secret generated Facebook.


### How to Get **appId** and **appSecret**
1. **Create a Facebook Developer Account:**
    - Go to the [Facebook Developers website](https://developers.facebook.com) and create a new app if you don't have one.
    - If you don't have a Facebook Developer account, you'll need to create one.
2. **Create a New App:**:  
    - Once logged in, navigate to the [Facebook App Dashboard](https://developers.facebook.com/apps).
    - Click on **Create App** to start a new app.
    - Choose Basic setting, then fill all fields like this picture below.  
    ![](/img/content/blog/facebook-oauth-setting.png)
3. Click **Save Changes**.
4. Done, now you already have the `appId` and `appSecret`.

:::info
- Facebook Oauth is required an URL with SSL.
- You must fill all the requirements field other than this.
- You must submit review to getting your app approved and published by Facebook.
:::

