---
sidebar_position: 3
---

# Sign-in With Github
OpenSSO support Sign in with Github. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  // ---
  github: {
    enable: false,
    client_id: 'XXXXXXXXXXXX',
    client_secret: 'XXXXXXXXXXXX'
  },
}
// ---
```

**Description:**
- **github.enable**  if set to true, then this feature will be activated.
- **github.client_id** is the client id generated from Github.
- **github.client_secret** is the client secret generated Github.


### How to Get **client_id** and **client_secret**
1. Go to [Github Developer Settings](https://github.com/settings/developers).
2. Under **OAuth Apps**, click **New OAuth App**.
3. Fill in the required details:
    - **Application name**: Your app's name.
    - **Homepage URL**: https://sso.yourdomain.com.
    - **Authorization callback URL**: https://sso.yourdomain.com.  
    ![](/img/content/blog/github-oauth-setting.png)
4. Click **Register Application**.
5. Done, now you already have the `client_id` and `client_secret`.

:::info
Github Oauth is required an URL with SSL.
:::

