---
sidebar_position: 2
---

# Sign-in With Google
OpenSSO support Sign in with Google. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  access_key: 'd2VsY29tZSB0byBvcGVuIHNzbw==',
  google: {
    enable: true,
    client_id: '606754042148XXXX.apps.googleusercontent.com',
    client_secret: 'XXXXX-XXXXX-XXXXX-XXXXX'
  }
}
// ---
```

**Description:**
- **google.enable**  if set to true, then this feature will be activated.
- **google.client_id** is the client id generated from Google Cloud Console.
- **google.client_secret** is the client secret generated from Google Cloud Console.

:::secondary
- **access_key** is the `access_key` to allow `request_token` for internal use.
- You can replace `access_key` with use random string whatever as you like.
- You have to get the client_id and client_secret.
:::

### How to Get **client_id** and **client_secret**
1. Go to [Google Credentials Page](https://console.cloud.google.com/apis/credentials).
2. Click Create credentials > OAuth client ID.
3. Select the `Web application` application type.
4. Name your `OAuth 2.0` client and click `Create`.
5. Done, now you already have the `client_id` and `client_secret`.

