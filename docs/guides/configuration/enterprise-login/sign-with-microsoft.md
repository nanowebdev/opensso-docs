---
sidebar_position: 3
---

# Sign-in With Microsoft
OpenSSO support Sign in with Microsoft Entra ID. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  // ---
  entra: {
    enable: false,
    clientId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
    clientSecret: 'xxxxx~xx~xxxxxxxxxx~x_xxxxxxxxxxxxxxxxx',
    redirectUri: 'https://yourdomain.com/sign-in',
    tenantId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx'
  }
}
// ---
```

**Description:**
- **entra.enable**  if set to true, then this feature will be activated.
- **entra.clientId** is the client id generated from Azure.
- **entra.clientSecret** is the client secret generated Azure.
- **entra.redirectUri** is the redirect/callback URI.
- **entra.tenantId** is the client secret generated Azure.


### How to Get **tenantId**, **clientId** and **clientSecret**
1. Register Your App in Microsoft Entra (formerly Azure AD)
  - Go to the [Microsoft Entra Portal](https://entra.microsoft.com).
  - Register a new application.
  - Set the redirect URI (e.g., https://yourdomain.com/sign-in). see picture below.  
  ![](/img/content/blog/microsoft-oauth-setting-1.png)

2. Click **Register**, now you will redirected to your app.
3. At the top right there is client credentials, click it to create new **client secret**.
4. Click **New Client Secret**. Fill all the fields requirement.
5. Now you already have the `tenantId`, `clientId` and `clientSecret`.
6. Last, you have to set your app permissions. see picture below.  
  ![](/img/content/blog/microsoft-oauth-setting-2.png)
7. Done

:::info
- Microsoft Entra ID is formerly Azure Active Directory.  
- Microsoft Oauth is required an URL with SSL.
:::

