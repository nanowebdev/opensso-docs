---
sidebar_position: 1
---

# Sign-in With Apple
OpenSSO support Sign in with Apple. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
// ---
oauth: {
  google: {
    // ...
  },
  apple: {
    enable: true,
    clientId: 'com.xxx.login',
    redirectURI: 'https://yourdomain.com'
  }
}
// ---
```

**Description:**
- **apple.enable**  if set to true, then this feature will be activated.
- **apple.clientId** is the client id generated from Apple Developer Console.
- **apple.redirectURI** is the location to which apple will redirect us after authentication is completed.

:::secondary
- You have to get the `clientId` and `redirectURI` from [Apple Developer](https://developer.apple.com).
- You require to have an apple developer license to use this feature.
:::

### How to get the `clientId` and `redirectURI`:
1. Open Your developer console and find `Certificates IDs & Profiles`.
2. Click the `plus symbol` to create a new `App ID`.
3. Once you had the `App ID` now go back to the developer console and `create a new identifier`. This time select `Services IDs`.
4. Once your new `Service ID` is created, find it then click on your `Service ID` that you just created.
5. You'll be greeted with a checkbox that say `Sign In with Apple`. now click on `Configure`.  
![](/img/content/doc-opensso/apple-1.png)
6. A modal will pop up,  
- As your Primary `App ID`, choose the one that we just created in the previous section.
- In the domains section add the domain where OpenSSO live, i.e `example.com`.
- The `Return URL` is the location to which apple will redirect us after authentication is completed. This is also the location where apple will send the user data after authentication. Apple will send a POST request to this URL, but OpenSSO using PopUp mode, so you just write it with yourdomain.com is enough. I've assigned http://example.com assumed that OpenSSO web is live on http://example.com.  
![](/img/content/doc-opensso/apple-2.png)

7. Click Next and Done.

