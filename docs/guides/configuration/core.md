---
sidebar_position: 1
---

# Core
This is the core configuration for server OpenSSO.

```js title="config.js"
const config = {
  // CORE
  // highlight-next-line
  host: '0.0.0.0', // Host Server (default is 0.0.0.0)
  // highlight-next-line
  port: 3000, // Port Server (default is 3000)
  // highlight-next-line
  logger: true, // Server Log (default is true)
  // highlight-next-line
  trustProxy: false, // Server will trust proxy (default is false)
  // highlight-next-line
  useWorker: false, // Use CPU as worker. Please don't use this if you're at shared hosting. (default is false)
  // highlight-next-line
  maxParamLength: 100, // Maximum url parameter query length (default is 100 characters)
  // ---
}
```

**Description:**
- **host** is the ip address for OpenSSO to be accessed, default is using **0.0.0.0**.
- **port** is the port for OpenSSO to be accessed, default is using **3000**.
- **trustProxy** is server will trust the proxy, if OpenSSO sitting behind nginx then you can set trustProxy to true. Default is false.  
- **logger** is the server log, if you set to true then it will log everything, good for debuging. Default is true.
- **useWorker** if set to true then OpenSSO will create multiple instance automatically by CPU count.
- **maxParamLength** is the maximum url parameter for query length. Default is 100 characters.

:::tip For Production
For production, you have to set logger to false to reduce the memory process.
:::

:::tip use Worker
Use worker will be a good choice if you run OpenSSO on single dedicated server or virtual private server.
:::

:::danger Don't use Worker
1. On docker environment.
2. On shared hosting.
:::