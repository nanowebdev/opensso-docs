---
sidebar_position: 3
---

# JWT 
OpenSSO use JWT for whole authentication. You have to generate new `private.key` and `public.key`, the easy way is you can grab it from [https://jwt.io](https://jwt.io). Make sure the algorithm is same.  

### **a. Config JWT**
Default JWT configuration is  
```js
jwtAlgorithm: 'RS256',
jwtExpiresIn: '8h', // 8 hours to expire
```

:::secondary
- It's better for you to use minimum 8 hours. Because we don't have a refresh token. When the token expired, they need to relogin.
:::

### **b. Generate New `private.key` and `public.key`**  
When you are in micro services, you have to use same `private.key` and `public.key`, so the token will verified in all nodes. 

To generate new `private.key` and `public.key`,  

1. Run this command below in your terminal
```bash
cd /path/app
chmod +x rs256jwt.sh
./rs256jwt.sh
```
2. Done, the old `private.key` and `public.key` file will be replaced automatically. 