---
sidebar_position: 4
---

# Cache
OpenSSO support to using multiple kind of cache engine. You can use `memory`, `file` and `redis`. But the default is using `memory` which is not working when application in clustered environment or in microservices.

:::tip Production
For best performance, scalable and works in all environments (clustered, containerized, microservices, etc), its best to use cache with Redis.
:::

Here is the example configuration:

### **a. Redis**  
```js title="config.js"
cache: {
  namespace: 'fastify',
  // highlight-next-line
  engine: 'redis',
  // highlight-next-line
  redisConn: 'redis://localhost:6379',
  //---
},
```

**Description:**  
- **namespace** is the environment cache name. This will help you to identify the OpenSSO cache in case if you're using Redis shared with other applications.
- **engine** is the cache type, you can choose `redis`, `file` and `memory`. Default is using `memory`.
- **redisConn** is the redis connection string, only work if the engine is redis. Default is redis://localhost:6379


### **b. Memory**  
```js title="config.js"
cache: {
  namespace: 'fastify',
  // highlight-next-line
  engine: 'memory',
  // highlight-next-line
  redisConn: '',
},
```

### **c. File**  
```js title="config.js"
cache: {
  namespace: 'fastify',
  // highlight-next-line
  engine: 'file',
  // highlight-next-line
  redisConn: '',
  // ---
},
```

### **d. TTL**
**TTL** is Time To Live. This will hold the cache lifetime. Default is 3600 (1 Hour).  
You can make it shorter or more longer.
```js title="config.js"
cache: {
  // ---
  ttl: {
    master_data: 3600,
    public_profile: 3600, // cache will auto release in 1 hour.
    total_user_month: 3600,
    total_user_year: 3600,
    total_user_active: 3600,
    total_user_inactive: 3600,
    total_user_list: 3600,
    menu_parent_list: 3600,
    menu_parent_group_list: 3600,
    menu_parent_role_list: 3600,
    menu_parent_role_list_grouped: 3600
  }
},
```