---
sidebar_position: 5
---

# Domain
You are able to change the domain, by set the `baseUrl` and `baseAssetsUrl`.

```js title="config.js"
const config = {
  // ---
  baseUrl: 'https://yourdomain.com',
  baseAssetsUrl: 'https://yourdomain.com',
  // ---
}
```

**Description:**
- **baseUrl** is the domain address for your website. should be full path with scheme `http://` or `https://`.
- **baseAssetsUrl** is refer to public directory. You can change this to your CDN path.

:::tip Domain
After you set the `baseUrl` and `baseAssetsUrl` with your domain, it will require a configuration with web server like `Nginx`, `Apache`, `HAProxy` etc.

But if you are using cPanel, it just work.
:::

:::secondary
If you want to run OpenSSO localy on your PC. Just set it like this.
```js title="config.js"
const config = {
  host: '0.0.0.0',
  port: 3000,
  // ---
  baseUrl: 'http://localhost:3000',
  baseAssetsUrl: 'http://localhost:3000',
  // ---
}
```
:::