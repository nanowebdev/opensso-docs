---
sidebar_position: 2
---

# II. Basic Hardening Server
In this guides, will not use any control panel system like cPanel, WebAdmin, etc.  
So we must setup some basic security to hardening our Ubuntu server.

:::tip
The const of using control panel:
- Mostly too bloated features, it could downgrade server performance.
- They running many services that not necessary to use.
- They forcing to update or upgrade which could lead breaking some production services.
- etc

Except if you know what you doing then just go with it.
:::

## 1. Change Default Port SSH  
SSH is the first gate for hacker to hijack your server. So its recommended to not use default SSH configuration.


### a. Check current port  
```bash
netstat -tulnp | grep ssh
```

### b. Edit Port  
```bash
nano /etc/ssh/sshd_config
```

Find the `Port` then change it to `2292`.
```bash title="Opened file: /etc/ssh/sshd_config"
# ---
Port 2292
# ---
```
Save it with press key `ctrl+x` then press `y` and `enter`.

:::secondary
This is just an example, you are able to use any port you like.  
Make sure there is no any services using the same port.
:::

### c. Restart SSH Service  
```bash
systemctl restart sshd
```

### d. Recheck current Port  
```bash
netstat -tulnp | grep ssh
```
If the listen port has changed to 2292. then your SSH port has been changed.

---

## 2. Disable root login SSH  
Ubuntu by default is allowing user root to login through SSH directly. So we must disabled it for better security. 

### a. Create new user  
```bash
adduser your_new_username
```
You will asked for the password after enter it.

Then you should exit from SSH.
```bash
exit
```

### b. Try to login SSH with new username
```bash
sudo ssh your_new_username@YOUR_IP -p2292
```

then login as root inside SSH
```bash
su
```

### c. Disable PermitRootLogin
:::danger
Don't do this, if you didn't successfully change from standard user to become root user.  
Or you will lose your server. 
:::

:::tip Skip this for now
It's better to disable PermitRootLogin on the end,  
when everything (OpenSSO) has been running well.

So let's skip this and go to **[Setup Fail2Ban](#3-setup-fail2ban)** now.
:::

If you are successfully login as root inside SSH, then edit `sshd_config`  
```bash
nano /etc/ssh/sshd_config
```

then look for line `PermitRootLogin` then change it to  
```bash title="Opened file: /etc/ssh/sshd_config"
# ---
PermitRootLogin no
# ---
```
Save it with press key `ctrl+x` then press `y` and `enter`.


### d. Restart SSH Service  
```bash
systemctl restart sshd
```

---

## 3. Setup Fail2Ban
Fail2Ban is a small security software to protect your server from brute force attack.

### a. Install Fail2Ban  
```bash
apt install fail2ban -y
```

### b. Start Fail2Ban Service  
```bash
systemctl start fail2ban
```

### c. Set auto start Fail2Ban
```bash
systemctl enable fail2ban
```
Now the Fail2Ban service will automatically started everytime when server rebooted.

### d. Create an empty jail.local file   
```bash
touch /etc/fail2ban/jail.local
```
    
### e. Open jail.local file
```bash
nano /etc/fail2ban/jail.local
```

then paste this example configuration for SSH port 2292
```bash title="Opened file: /etc/fail2ban/jail.local"
[sshd]
enabled = true
port = 2292
bantime = 3600
findtime = 600
maxretry = 3
logpath = /var/log/fail2ban.log
```
Save it with press key `ctrl+x` then press `y` and `enter`.

### f. Restart Fail2Ban service  
```bash
systemctl restart fail2ban
```

Now you have Fail2Ban service running. When someone trying to login with wrong password for 3x, then his IP address will get banned for 1 hour.

To check IP that get banned by Fail2Ban  
```bash
fail2ban-client status sshd
```

To Unban IP  
```bash
fail2ban-client set YOURJAILNAMEHERE unbanip IPADDRESSHERE
```

---

## 4. UFW Firewall
UFW is installed by default on Ubuntu. So let's setup it.

### a. Check Status
```bash
ufw status
```

If you've seen
```bash title="output"
ufw: command not found
```

It means UFW is not installed yet, you can install it by run this command.
```bash
apt install ufw
```

### b. Activate UFW
To activate UFW, please run this command.
```bash
ufw enable
```

### c. Configure UFW
We need to know list app available for ufw to allow.

```bash
ufw app list
```

You will see
```bash title="output"
Available applications:
  OpenSSH
```

If you want to use default Port for SSH.
```bash
ufw allow ssh
```

But because we using custom port for SSH 2292, then we should add it manually.
```bash
ufw allow 2292/tcp
```

### d. Check status
Now we should check the status again.
```bash
ufw status
```

then you will see like this
```bash title="output"
Status: active

To                         Action      From
--                         ------      ----
2292/tcp                   ALLOW       Anywhere                  
2292/tcp (v6)              ALLOW       Anywhere (v6) 
```

:::tip
Now your ubuntu server is open for port 2292 only.  
All ports except 2292 will absolutely closed and rejected.
:::

## 5. Congratulations
Now your ubuntu server already secured.