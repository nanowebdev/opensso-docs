---
sidebar_position: 1
---

# Get Started

Let's discover **OpenSSO in less than 5 minutes**.

## Getting Started

OpenSSO is a NodeJS application. Make sure you’ve done already to install the NodeJS server on your server or on your local computer.

### A. Requirement

Minimum requirement to install OpenSSO project:
- NodeJS min v.16.20.0
- Database [Optional]
- Redis 3 [Optional]

:::tip
To make OpenSSO could stable longer,  
here is the recommended stacks:
- NodeJS 18 or higher
- Database PostgreSQL
- Redis 7
:::

:::info
- The default database is using SQLite, but you can switch to another database like MariaDB, MySQL or PostgreSQL for easy monitoring, maintaining and scaling in the future.
- The default cache engine is using memory, but you can switch to Redis easily from config file. Using Redis is optional but it will become required if your application working on container service, clustered or behind the proxy load balancer.
:::

:::danger Important
Since OpenSSO version 1.3.0, OpenSSO will not to support NodeJS v14 anymore. If you still wanted to use NodeJS v14, just keep using OpenSSO version 1.2.0.
:::


### B. Restore Database [Optional, you can skip this part]
This is optional, OpenSSO using SQLite as default and all features works well with it. You need to setup database if you have a plan to run OpenSSO using MariaDB or MySQL database server. You can skip this if `SQLite` is enough for you. 

1. Install `MariaDB` or `MySQL`. You can search how to install this by using Google search engine.
2. Create a New Database, then execute or import the file `db.mysql.sql` located inside directory `database` into your database.  
  See the picture below.  
  ![](https://i.imgur.com/CGBVayx.png)

3. Edit `config.js` like this below
```js title="config.js"
  sequelizeOption: {
    // highlight-next-line
    dialect: 'mariadb', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
    // highlight-next-line
    dbname: 'DATABASE_NAME',
    // highlight-next-line
    username: 'DATABASE_USER_NAME',
    // highlight-next-line
    password: 'DATABASE_USER_PASSWORD',
    // highlight-next-line
    options: {
      // highlight-next-line
      host: 'DATABASE_HOST_OR_SERVER',
      // highlight-next-line
      port: 'DATABASE_PORT'
    // highlight-next-line
    }
    // dialect: 'sqlite',
    // storage: './database/data.sqlite3'
  },
```
:::tip
- If you having trouble with MariaDB, try using dialect `mysql`.
- File `db.mysql.sql` is only works for `MariaDB` and `MySQL` only.
- File `db.postgre.sql` is only works for `PostgreSQL` only.
:::

:::secondary
Working with `MSSQL` or `Oracle` should works too, but we don't create the database structure for it,  
so you have to create it by yourself.


But if in some reason you want to play with MSSQL or Oracle,  
you have to install this additional package:
- **MSSQL** >> npm install tedious
- **Oracle** >> npm install oracledb
:::

4. Done


### C. Build App and Run it
You have to build the app for the first time.  
1. Go to the directory path then typing
    ```
    npm install
    ```
2. Wait until the process complete.

:::secondary
As you can see, there is only 1 deprecated package. 
```bash
npm WARN deprecated @npmcli/move-file@1.1.2: This functionality has been moved to @npmcli/fs
```

It's from sqlite3 dependency. You can ignore this for now since it's already the latest version and there is no fix available for NodeJS 14.
:::

:::tip
If you're using NodeJS v16, then you can just run
```bash
npm update
```
Then it will fixed it.  

**Note:**  
Don't do `npm update` if you're still using NodeJS 14.
:::

3. Then you can run the app by typing
    ```
    node server.js
    ```
    or by typing
    ```bash
    npm start
    ```

4. Now, Open the url [http://localhost:3000](http://localhost:3000).

:::secondary
- There is no default account, so when you're running this application for the first time, you have to register a new account to get the `admin` role.
:::


### Congratulations!

OpenSSO succesfully running in your local computer. If OpenSSO run in your local computer, means it will work too on the live server.

Let's try to learn more deep, you could deploy it later.


## What's next?

- Learn how to configure OpenSSO, Please read [Configuration](/docs/category/configuration).
- Deploy OpenSSO into cPanel, Please read [Deploy to cPanel](/docs/guides/deployment/deploy-to-cpanel).
- Deploy OpenSSO to Ubuntu, Please read [Deploy to Ubuntu](/docs/category/deploy-on-ubuntu).
- Deploy OpenSSO to Ubuntu Full Guides, Please read [Deploy to Ubuntu (FULL)](/docs/category/deploy-on-ubuntu-full).
- Let's play around with our [APIs](/docs/category/api).