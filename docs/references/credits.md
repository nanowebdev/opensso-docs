---
sidebar_position: 3
---

# Credits
All OpenSSO libraries or external scripts is using an open source library.

Frontend we using:
- [Argon Dashboard 2](https://www.creative-tim.com/product/argon-dashboard)
- [Bootstrap 5](https://getbootstrap.com)
- [reCaptcha V3](https://developers.google.com/recaptcha/docs/v3)
- [Dom.js](https://github.com/aalfiann/dom.js)
- [Axios Http](https://axios-http.com)
- [Browser Storage Class](https://gitlab.com/aalfiann/browser-storage-class)
- [Fly Json ODM](https://github.com/aalfiann/fly-json-odm)
- [Momentjs](https://momentjs.com)
- [Native Form Validation](https://github.com/aalfiann/native-form-validation)
- [ReefJS V12](https://reefjs.com/v12)
- [Sweetalert](https://sweetalert.js.org)

:::secondary
- For backend, you can see the dependencies at `package.json`.
- There is no jQuery in this application, but you are able to add it by yourself if you need it. 
:::
