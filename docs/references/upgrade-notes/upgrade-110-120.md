---
sidebar_position: 2
---

# Upgrade 1.1.0 - 1.2.0
This upgrade theres some files has changed even break changes happened. Just replace it will be more easier, but if you have modified OpenSSO code by yourself, then you should consider about these files:

```bash title="File New"
root
├── public
│   └── assets
│       └── default
│           └── js
│               ├── argon-switcher.js
│               └── argon-switcher.min.js
├── public
│   └── assets
│       └── inverse/*
├── views
│   └── inverse/*
├── lib
│   └── base64.js
├── test
│   └── base64.js
├── Dockerfile
├── docker-compose.yml
.
```

```bash title="File Changes"
root
├── database
│   ├── db.mysql.sql
│   └── db.postgre.sql
├── public
│   └── assets
│       └── default
│           └── css
│               └── argon-dashboard.css
├── public
│   └── assets
│       └── default
│           └── js
│               ├── argon-dashboard.js
│               └── argon-dashboard.min.js
├── views
│   └── default/*
├── routes
│   └── user.js
├── schemas
│   └── user.js
├── config.default.js
├── config.js
├── server.js
├── package-lock.json
├── package.json
.
```

```bash title="File Removed"
root
├── public
│   └── assets
│       └── default
│           └── img
│               └── curved-images/*
.
```

:::secondary  
- If you see a wildcard, it means all files were added, modified or maybe removed.
:::


## How to manually Upgrade ?  
Please attention, in this upgrade, the template engine has been migrated to the new version and it cause break changes. Means your current theme will breaks. So you need to be more carefully to follow this upgrade steps.

:::danger Alwways Backup
**Please Remember to always backup before doing any upgrades.**
:::

:::tip Try on local
To increase the successful possibilty when doing upgrading, always try on your local computer first.
:::

### 1. Modify the template, your template is on directory `views/default`.  
Use search tools on vscode, try to search `<%~ includeFile(`, and then replace all with `<%~ include(`.

### 2. Modify the `config.js`,  
Add new configuration property name `logoEmail`.  
  ```js title="Edit file config.js"
    // ...
    // Mailer
    // useMailer will affect on contact form page
    useMailer: true,
    // Image logo for email (max width size is set as 200px and you must put full path url image) 
    logoEmail: 'https://i.imgur.com/UJYGvee.png',
    // ...
  ```

### 3. Modify the `server.js`,  
  ```js title="Edit file server.js"
    // Remove the redis const
    // const redis = require('redis') 

    // Add new const for Eta at the top.
    // ...
    require('make-promises-safe')
    const { Eta } = require('eta')
    // ...
    
    // Then modify the engine of eta
    // ...
      server.register(require('@fastify/view'), {
        engine: {
          eta: new Eta()
        },
    // ...
    
    // Then modify the fastify-cacheman plugin  
    // ...
    server.register(require('fastify-cacheman'), {
      // ...
      client: (config.cache.engine === 'redis' ? config.cache.redisConn : '')
    })
  ```

### 4. Replace the old `package-lock.json` and `package.json`.
Just replace it with newer file from this version.

### 5. Delete current `node_modules` directory.

```bash title="Then run this command"
npm install
```
It will automatically installing the new package and dependencies.  

:::secondary
As you can see, there is only 1 deprecated package. 
```bash
npm WARN deprecated @npmcli/move-file@1.1.2: This functionality has been moved to @npmcli/fs
```

It's from sqlite3 dependency. You can ignore this for now since it's already the latest version and there is no fix available for NodeJS 14.
:::

:::tip
If you're using NodeJS v16, then you can just run
```bash
npm update
```
Then it will fixed it.  

**Note:**  
Don't do `npm update` if you're still using NodeJS 14.
:::

### 6. Update the database collumn for `email`.  
Look for `email` field with type varchar and change it length from 100 to become 254.  
Do this on table `users` and `lastlogins`. 

### 7. Add new theme named inverse [Optional].  
:::info
If you're prefer to use your own theme or don't want to use this new theme, then you can skip this.
:::

Here is how to add new inverse theme, manually:  
  a. Add new directory `public/assets/inverse`.  
  b. Copy the directory `public/assets/inverse` from this version and paste to the new created directory `inverse`.  
  c. Add new directory `views/inverse`.  
  d. Copy the directory `views/inverse` from this version and paste to the new created directory `inverse`.

### 8. Done
Now you can try to run the OpenSSO application.

```bash
node server.js
```

or

```bash
npm start
```

