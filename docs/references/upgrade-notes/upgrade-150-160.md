---
sidebar_position: 6
---

# Upgrade 1.5.0 - 1.6.0
In this version, there is break changes happened because OpenSSO now adding more social logins which is many things are changed in views template and there is some new files added. **Just replace it, will be more easier**, but if you have already modified OpenSSO code by yourself, then you should consider about these files:

```bash title="File New"
root
├── public/
│   └── assets/
│       └── vendor/
│           └── fontawesome-free-6.6.0-web/*
├── views/
│   └── default/
│       └── user-deletion-policy.html
├── views/
│   └── inverse/
│       └── user-deletion-policy.html
.
```

```bash title="File Changes"
root
├── routes/
│   ├── oauth.js
│   ├── page.js
│   └── user.js
├── schemas/
│   └── oauth.js
├── views/
│   └── default/
│       ├── backend/
│       │   ├── _global_js_.html
│       │   ├── _header.html
│       │   ├── _layout.html
│       │   ├── _layout_profile.html
│       │   ├── _menu_aside.html
│       │   ├── dashboard.html
│       │   └── my_profile.html
│       ├── _footer.html
│       ├── _global_js.html
│       ├── _header.html
│       ├── _layout.html
│       ├── _navbar.html
│       ├── about.html
│       ├── contact.html
│       ├── sign-in.html
│       └── sso-login.html
├── views/
│   └── inverse/
│       ├── backend/
│       │   ├── _global_js_.html
│       │   ├── _header.html
│       │   ├── _layout.html
│       │   ├── _layout_profile.html
│       │   ├── _menu_aside.html
│       │   ├── dashboard.html
│       │   └── my_profile.html
│       ├── _footer.html
│       ├── _global_js.html
│       ├── _header.html
│       ├── _layout.html
│       ├── _navbar.html
│       ├── about.html
│       ├── contact.html
│       ├── sign-in.html
│       └── sso-login.html
├── changelog.md
├── config.default.js
├── config.js
├── package-lock.json
├── package.json
.
```

## How to manually Upgrade ?  
Please attention, in this upgrade, there is a small break changes. So you need to be careful to follow this upgrade steps.

:::danger Always Backup
**Please Remember to always backup before doing any upgrades.**
:::

:::tip Try on local
To increase the successful possibilty when doing upgrading, always try on your local computer first.
:::


### 1. Modify your `config.js`

Add some social logins configuration in your `config.js`.  
Its located after `apple`.

```js title="config.js"
{
  // ...
  {
    // ...
  	google: {
      // ...
    },
  	apple: {
      // ...
    },
	// highlight-next-line
    github: {
	  // highlight-next-line
      enable: false,
	  // highlight-next-line
      client_id: 'XXXXXXXXXXXX',
	  // highlight-next-line
      client_secret: 'XXXXXXXXXXXX'
	// highlight-next-line
    },
	// highlight-next-line
    gitlab: {
	  // highlight-next-line
      enable: false,
	  // highlight-next-line
      client_id: 'XXXXXXXXXXXX',
	  // highlight-next-line
      client_secret: 'XXXXXXXXXXXX'
	// highlight-next-line
    },
	// highlight-next-line
    facebook: {
	  // highlight-next-line
      enable: false,
	  // highlight-next-line
      appId: 'XXXXXXXXXXXX',
	  // highlight-next-line
      appSecret: 'XXXXXXXXXXXX'
	// highlight-next-line
    },
	// highlight-next-line
    x: {
	  // highlight-next-line
      enable: false,
	  // highlight-next-line
      consumer_api_key: 'XXXXXXXXXXXX',
	  // highlight-next-line
      consumer_api_secret: 'XXXXXXXXXXXX',
	  // highlight-next-line
      client_id: 'XXXXXXXXXXXX',
	  // highlight-next-line
      client_secret: 'XXXXXXXXXXXX'
	// highlight-next-line
    },
	// highlight-next-line
    entra: {
	  // highlight-next-line
      enable: false,
	  // highlight-next-line
      clientId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
	  // highlight-next-line
      clientSecret: 'xxxxx~xx~xxxxxxxxxx~x_xxxxxxxxxxxxxxxxx',
	  // highlight-next-line
      redirectUri: 'https://yourdomain.com/sign-in',
	  // highlight-next-line
      tenantId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx'
	// highlight-next-line
    }
  },
  // ...
}

module.exports = config
```

### 2. Replace the old `package-lock.json` and `package.json`.
Just replace it with newer file from this version.

### 3. Delete current `node_modules` directory.

```bash title="Then run this command"
npm install
```
It will automatically installing the new package and dependencies.  

### 4. Done
Now you can try to run the OpenSSO application.

```bash
node server.js
```

or

```bash
npm start
```

