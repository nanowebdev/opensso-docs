// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'OpenSSO',
  tagline: 'An User Management System with Single Sign On.',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://opensso.nanowebdev.eu.org',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  // projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          includeCurrentVersion: true,
          lastVersion: 'current',
          versions: {
            current: {
              label: '1.6.0 (latest)',
              path: '',
            },
          },
          // 
          // lastVersion: '1.4.0',
          // versions: {
          //   current: {
          //     label: '1.5.0 (upcoming)',
          //     path: 'upcoming',
          //   },
          //   '1.4.0': {
          //     label: '1.4.0 (latest)',
          //     path: '',
          //   },
          // },
          //
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/opensso-social-card.jpg',
      navbar: {
        title: 'OpenSSO',
        logo: {
          alt: 'OpenSSO Logo',
          src: 'img/logo.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Docs',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            type: 'docsVersionDropdown', position: 'right'
          },
          {
            href: 'https://codecanyon.net/item/open-sso-single-sign-on-nodejs/44234269',
            label: 'Get OpenSSO',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Get Started',
                to: '/docs/get-started',
              },
              {
                label: 'Guides',
                to: '/docs/category/guides'
              }
            ],
          },
          {
            title: 'Updates',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'Changelog',
                to: '/docs/references/changelog'
              },
            ],
          },
          {
            title: 'Resources',
            items: [
              {
                label: 'Get OpenSSO',
                to: 'https://codecanyon.net/item/open-sso-single-sign-on-nodejs/44234269',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/nanowebdev',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} NanoWebDev.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
