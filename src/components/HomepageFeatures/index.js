import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Json Web Token',
    src: require('@site/static/img/jwt.png').default,
    description: (
      <>
        OpenSSO is a centralized user management system that using Single Sign On (SSO) for authentication method.
        OpenSSO follow the JWT standard for Oauth 2.0 as describe in RFC7519 and RFC9068. 
      </>
    ),
  },
  {
    title: 'Support Multiple Databases',
    src: require('@site/static/img/sequelize-200.png').default,
    description: (
      <>
        The default database is using SQLite, but you can switch to another database like MariaDB, MySQL or PostgreSQL for easy monitoring, maintaining and scaling in the future.
      </>
    ),
  },
  {
    title: 'First Rest API',
    src: require('@site/static/img/api-rest.png').default,
    description: (
      <>
        OpenSSO is First Rest API architecture. It will easier for you to redesign the template and custom integration SSO system.
      </>
    ),
  },
];

function Feature({src, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img className={styles.featureSvg} role="img" src={src} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
