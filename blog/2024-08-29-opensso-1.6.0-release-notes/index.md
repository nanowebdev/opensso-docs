---
slug: opensso-1.6.0-release-notes
title: OpenSSO 1.6.0 Release Notes
authors: [aalfiann]
tags: [information, new-release]
---

OpenSSO 1.6.0 Release Notes.  

![](/img/content/blog/social-login.png)

### What's new in this 1.6.0:
<!--truncate-->
**1. Added some of social logins.**  
Added more social logins like `Github`, `Gitlab`, `Facebook`, `Twitter X` and `Microsoft Entra ID`.

**2. Added new deletion policy page**  
Facebook platform is require a deletion policy page url to register your app for integration.

### Issues that have been resolved ?
**1. Fixed typo in file `db.mysql.sql`**  
We found that some users is failing to import default mysql database file.

**2. Fixed fontawesome doesn't displayed correctly**  
We found that some icon and button which is using icon is disappeared.

**3. Fixed baseAssetsUrl doesn't working as expected**  
baseAssetsUrl is a configuration to set assets url. i.e if you want to use assets using CDN.

**4. Fixed Total User List in yearly**  
We found that calculation in total user list API is not accurate.

### Should I Upgrade ?  
**Yes**, because icon doesn't load means some button were dissappearing.  
Some function might be not working. So this is important.

### How to Upgrade ?
Here is the Guide about **[How to Upgrade from 1.5.0 to 1.6.0](/docs/references/upgrade-notes/upgrade-150-160)**.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
