---
slug: opensso-1.4.0-release-notes
title: OpenSSO 1.4.0 Release Notes
authors: [aalfiann]
tags: [information, new-release]
---

OpenSSO 1.4.0 Release Notes.  

![](/img/content/blog/2fa-mfa-opensso.png)

### What's new in this 1.4.0:
<!--truncate-->
**1. Now support 2FA/MFA via Email and/or Authenticator.**  
Already implemented 2FA/MFA at user level. User could activate it as 2FA or MFA.

2FA (Two-Factor Authentication) and MFA (Multi-Factor Authentication) are security processes that enhance the protection of accounts and sensitive information by requiring multiple forms of verification before granting access.

**Benefits of 2FA and MFA**

- **Enhanced Security:** By requiring more than one form of verification, it becomes significantly harder for unauthorized users to gain access.
- **Reduced Risk of Fraud:** Even if one factor (like a password) is compromised, additional layers of security prevent access.
- **Compliance:** Many regulations and standards (like GDPR, HIPAA) require or recommend the use of MFA to protect sensitive information.

**Notes about 2FA/MFA in OpenSSO:**
- 2FA is using TOTP which is follow [RFC6238](https://www.rfc-editor.org/rfc/rfc6238).
- Supported to be used with Google Authenticator, Microsoft Authenticator, Authy, etc.
- It is optional, user could activate it or not.
- User could manage it, including register, update and reset.
- Admin could force reset 2FA users.
- 2FA is optional, website owner could disable it through configuration if the external apps is not ready to handle 2FA.

**2. Now support use a custom id and/with hash value when registering new user**  
The purpose of this update is to make more easier to import the existing user from old applications into OpenSSO.

So now you can directly register new users with same ID and hash password from old application:
- ID
- Hash password

Notes:
- OpenSSO is using Bcrypt with rounds 10 as default. If this algorithm is different with your old application, then you can skip it to not use same hash password.

**3. Upgrade fastify to version 4.27.x**  
OpenSSO will always sticky to the latest of fastify framework.

**4. Upgrade Dependency**  
There is many upgrade dependencies in this version.

### Issues that already resolved ?
**1. There is no way to insert user from existing apps**  
A register API there is no way to insert with custom ID, but this already solved in this version. 

**2. Can't reply directly to the incoming mail from contact page**  
It's already been solved in this version.

**3. Simplify email content to avoid spam trigerred issue**  
In some email provider, the email content got spam trigerred. So I tried to modified the html template for email, 
to make sure there is no spam trigerred issue in the future. 

**4. Deprecated Dependencies**  
Some dependencies already upgraded to the latest version. Preparing for the incoming upgrade OpenSSO V2.

**5. Backwards Compatibility Config**  
Config is too strict, start from this version, a new added config is unstrict (optional).

### Should I Upgrade ?
This upgrade is important if:
- You want to improve the security to the user by activating 2FA. If not, you can ignore this upgrade.
- By following this upgrade, you're ready to the incoming big update OpenSSO V2.

### How to Upgrade ?
Here is the Guide about **[How to Upgrade from 1.3.0 to 1.4.0](/docs/references/upgrade-notes/upgrade-130-140)**.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
