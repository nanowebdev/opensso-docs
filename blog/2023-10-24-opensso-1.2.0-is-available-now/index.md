---
slug: opensso-1.2.0-is-available-now
title: OpenSSO 1.2.0 is Available Now
authors: [aalfiann]
tags: [information, new-release]
---

OpenSSO 1.2.0 is available now.  

### What's new in this 1.2.0:
<!--truncate-->
**1. New Inverse Theme**  
This is same theme with default theme, but only differents in color.  
Inverse theme using dark and light color which is dark is the default color.

**2. Color Switcher**  
This is a feature to enable your user to swith the theme color.

**3. Email Logo**  
Now you can put your logo into email content.

**4. Support Docker and Docker Compose**  
Now OpenSSO support to deploy and run inside the Docker container.

**5. Migration ETA Template Engine to version 3**  
ETA has been rewrite their template engine core to be more extendable. Note, this migration will makes your old template obsolete.

**6. Username and Email address**  
Username and email address in OpenSSO now follow [RFC 5321](https://www.rfc-editor.org/rfc/rfc5321#section-4.5.3.1.1).

**7. Upgrade Dependency**  
There is many upgrade dependencies in this version.

### Issues that already resolved ?
**1. Deprecated Dependencies**  
All critical and deprecated dependencies already upgraded to the latest version. This also fixed the securities issues from the packages.

**2. Username too short**  
I found that OpenSSO username put a limit to 20 chars. This will cause a problem to someone that using longer email address on oAuth Google or Apple.

**3. There is no way to change the logo on email**  
I've already add the logoLink into `config.js` so you can easier to put the logo on email.

### Should I Upgrade ?
This upgrade is important if you're using oAuth Google or Apple. If not, you can ignore this upgrade.

### How to Upgrade ?
Here is the Guide about **[How to Upgrade from 1.1.0 to 1.2.0](/docs/references/upgrade-notes/upgrade-110-120)**.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
