---
slug: welcome-to-the-new-docs
title: Welcome to The New Docs
authors: [aalfiann]
tags: [information]
---

Starting today, OpenSSO online documentation will moved to here.  

The reasons are:
1. For better versionized the docs.
2. User could focus on it's project documentation.
3. To separate OpenSSO and NanoWebDev information.

Thank you so much for supporting by buying our scripts.  
I'll try to deliver my best application in the future.
