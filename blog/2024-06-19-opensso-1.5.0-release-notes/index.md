---
slug: opensso-1.5.0-release-notes
title: OpenSSO 1.5.0 Release Notes
authors: [aalfiann]
tags: [information, new-release]
---

OpenSSO 1.5.0 Release Notes.  

### What's new in this 1.5.0:
<!--truncate-->
**1. Add whitelist for better security layer to SSO.**  
Now your SSO page login is protected by giving it a whitelist request url.  
This feature is optional. Default is inactive.

**2. Add webhook for broadcasting/event streaming support**  
Now you could activate a webhook in your SSO.  
We will sent the successful login event only.

Benefit of using Webhook:
- All of your applications could listen to the login event of your users.
- You can log the status login of your users.

**3. New API to import data user**  
Now we already added new API for handling import data user which is more safe, faster and flexible.

### Issues that already resolved ?
**1. Using Register API for importing an existing user is not safe**  
A register API should not for doing import. A bad person could spam it by injecting a lot of user data.

**2. Access Token now implemented on Login API**  
To reduce spam and giving more security protection, now using API to login is required an Access Token header.

**3. Fixed apple signin issue**  
There is no any information detail when apple signin is failing because their browser is blocking a popup.

**4. No longer auto generate key when updating SSO**  
Updating data SSO should not auto generating new SSO key. This could be hard to maintain many apps when we don't have a control to maintain SSO Key.

**5. Fixed some bugs on ui**  
We find some bugs on ui and already fix it. 

### Should I Upgrade ?
This upgrade is important if:
- Yes, in this version is focusing on security.
- By following this upgrade, you're ready to the incoming big update OpenSSO V2.

### How to Upgrade ?
Here is the Guide about **[How to Upgrade from 1.4.0 to 1.5.0](/docs/references/upgrade-notes/upgrade-140-150)**.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
