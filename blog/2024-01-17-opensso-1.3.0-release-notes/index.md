---
slug: opensso-1.3.0-release-notes
title: OpenSSO 1.3.0 Release Notes
authors: [aalfiann]
tags: [information, new-release]
---

OpenSSO 1.3.0 Release Notes.  

### What's new in this 1.3.0:
<!--truncate-->
**1. No longer to support NodeJS 14**  
OpenSSO will not support NodeJS 14 anymore. The minimum requirement to run OpenSSO is NodeJS v16. 
We keep it support NodeJS version as low as possible to reach many cheap hosting provider. 

But, using NodeJS version higher than v16 is always recommended.

**2. More flexible to Register New User via API**  
Now support adding custom field on user data when registering new user via API.

The common field for register new user is:
- username
- email

But now you can more flexible to add or inject more data to your customer when they register to OpenSSO.

**3. Upgrade fastify to version 4.25.x**  
OpenSSO will always sticky to the latest of fastify framework.

**4. Upgrade Dependency**  
There is many upgrade dependencies in this version.

### Issues that already resolved ?
**1. Deprecated Dependencies**  
All critical and deprecated dependencies already upgraded to the latest version. This also fixed the securities issues from the packages.

**2. Fixed updating user will not always replacing old custom data field**  
The current situation is when updating current old data custom field, it always replacing the whole object.
We can't update only at spesific key name object.

**3. Fixed Sign-in with Apple is not working on sign-in page**  
User getting stuck when trying to sign-in with Apple ID.

**4. Fixed some browser keep inserting username on dashboard panel**  
Some modern browser always detect current username and they keep it automatically inserted on any textbox in dashboard panel.

### Should I Upgrade ?
This upgrade is important if:
- You're using oAuth Apple. If not, you can ignore this upgrade.

### How to Upgrade ?
Here is the Guide about **[How to Upgrade from 1.2.0 to 1.3.0](/docs/references/upgrade-notes/upgrade-120-130)**.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
