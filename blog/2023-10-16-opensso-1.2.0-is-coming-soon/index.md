---
slug: opensso-1.2.0-is-coming-soon
title: OpenSSO 1.2.0 is Coming Soon
authors: [aalfiann]
tags: [information, planning]
---

The release of OpenSSO 1.2.0 will be available coming soon.

In this new version will be brings:

<!--truncate-->

**1. New Inverse Theme**  
This is same theme with default theme, but only differents in color.  
Inverse theme using dark and light color which is dark is the default color.

**2. Color Switcher**  
This is a feature to enable your user to swith the theme color.

**3. Email Logo**  
Now you can put your logo into email content.

**4. Support Docker and Docker Compose**  
Now OpenSSO support to deploy and run inside the Docker container.

**5. Migration ETA Template Engine to version 3**  
ETA has been rewrite their template engine core to be more extendable.

**6. Username and Email address**  
Username and email address in OpenSSO now follow [RFC 5321](https://www.rfc-editor.org/rfc/rfc5321#section-4.5.3.1.1).

**7. Upgrade Dependency**  
There is many upgrade dependencies in this version.

:::danger BREAK Changes
There is a Break Changes in this version.  
Make sure you follow the upgrade step carefully.
:::
