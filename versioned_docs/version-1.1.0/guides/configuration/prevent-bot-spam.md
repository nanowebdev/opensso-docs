---
sidebar_position: 7
---

# Prevent BOT / SPAM
OpenSSO could prevent bot / spam, you have to activate this feature.

```js title="config.js"
const config = {
  // ---
  recaptchaSiteKey: 'xxxxxxxxxxx',
  recaptchaSecretKey: 'xxxxxxxxxxx',
  hideRecaptchaBadge: true,
  // ---
}
```

**Description:**
- **recaptchaSiteKey** is the Site Key generated from reCaptcha.
- **recaptchaSecretKey** is the Secret Key generated from reCaptcha.
- **hideRecaptchaBadge** if set to true, then will display the icon reCaptcha on the UI.

:::secondary
- OpenSSO using reCaptcha v3.
- You have to get the reCaptcha SiteKey and SecretKey at **[here](https://www.google.com/recaptcha/admin)**.
- This reCaptcha will work only at sending email on contact page.
:::
