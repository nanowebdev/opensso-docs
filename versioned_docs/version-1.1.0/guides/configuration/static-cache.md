---
sidebar_position: 9
---

# Static Cache
Static cache is different with [Cache](/docs/guides/configuration/cache). This Static cache only cache the page that rendered by template engine and file static located inside public directory.


```js title="config.js"
// ---
maxAgeStaticCache: (3600 * 24),
maxAgeAssetsCache: (3600 * 24 * 30)
// ---
```

**Description:**
- **maxAgeStaticCache** is cache for regular static pages (default is 1 day in seconds).
- **maxAgeAssetsCache**  is cache for assets ( default is 30 days in seconds).

:::secondary
- NGINX or other HTTP Proxy will follow this rule by default. You don’t have to create custom cache configuration to handle this.
:::
