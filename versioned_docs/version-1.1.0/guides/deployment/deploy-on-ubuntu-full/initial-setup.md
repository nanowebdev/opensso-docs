---
sidebar_position: 1
---

# I. Initial Setup
Before we start going to use the server for production use, make sure we setup the ubuntu server first.

In this guide, I will use Ubuntu LTS 20.14 and of course my ubuntu is fresh installed.

:::info Requirement
Before you following all this guides make sure you have:
1. A VPS or Dedicated Server with fresh installed Ubuntu 20.14.
2. A Domain Name.
3. A Cloudflare account to manage DNS. 
4. An OpenSSO with minimum version 1.2.0.
:::

:::secondary
All this guides will use,
- **123.123.123.123** as public ip address.
- **yourdomain.com** as domain name.

You should rename this with your ip address and domain name.
:::

:::danger Disclaimer
Your server must be fresh installed.  
Because, I won't responsible with your data on server.
:::

## 1. Pointing your domain to Cloudflare.
This guide will not explain detail how to do this. You should try google search how to register yourdomain.com into Cloudflare.  

After you successfully pointing yourdomain.com into cloudflare, then go to DNS section on cloudflare.  
Now we must setup the DNS for your server and mail server.

### a. Add yourdomain pointing to your server
|Type|Name|Content|Proxy Status|
|---|---|---|---|
|A|@|123.123.123.123|Proxied|

### b. Add CNAME pointing to yourdomain.
|Type|Name|Content|Proxy Status|
|---|---|---|---|
|CNAME|www|yourdomain.com|DNS Only|

### c. Add sub domain sso for OpenSSO
|Type|Name|Content|Proxy Status|
|---|---|---|---|
|A|sso|123.123.123.123|Proxied|

### d. Add mail pointing to your server
|Type|Name|Content|Proxy Status|
|---|---|---|---|
|A|mail|123.123.123.123|DNS Only|

### e. Add MX pointing to mail.yourdomain.com
|Type|Name|Content|Proxy Status|Priority|
|---|---|---|---|---|
|MX|yourdomain.com|mail.yourdomain.com|DNS Only|10|


:::tip Done
Now the next step we continue to setup Ubuntu Server.
:::

## 2. Login to your server using SSH
Using termminal
```bash
sudo ssh root@123.123.123.123 -p22
```

If you're using windows, you can try using Putty.

## 3. Set Password  
This is an optional, but sometimes change password would made yourself convenient to remember about how to connect into your server using SSH.  
```bash
passwd
```

## 4. Set Hostname  
Make sure you have already set your hostname and hosts

### a. Edit Hostname
```bash
nano /etc/hostname
```

Delete all, then put like this
```bash title="Opened file: /etc/hostname"
mail.yourdomain.com
```

Then save it by press `ctlr+x` then press `y` and `enter`.

:::secondary
- Replace `mail.yourdomain.com` with real your domain.
:::

### b. Edit your hosts
```bash
nano /etc/hosts
```

Make sure your hosts is like this
```bash title="Opened file: /etc/hosts"
127.0.0.1       localhost
127.0.1.1       mail.yourdomain.com
123.123.123.123 mail.yourdomain.com

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

Then save it by press `ctlr+x` then press `y` and `enter`.

:::secondary
- Add `127.0.0.1 mail.yourdomain.com`.
- Replace `mail.yourdomain.com` with real your domain.
- Replace `123.123.123.123` with your public ip address.
- Leave all settings for IPv6 as default.
:::

:::tip Info
Why we set it as sub domain for **mail** ?  
Because we will use this server as mailer too.
:::

## 5. Update and Upgrade  
Make you already using the latest package repository.
```bash
apt update
apt upgrade
```

## 6. Set Timezone  
This is required to avoid miss information about date for SSL, Mailer and Database.
```bash
dpkg-reconfigure tzdata
```

## 7. Reboot  
We need to reboot to take the effect.
```bash
reboot
```

Now you will automatically exiting from SSH connection.  
Wait for 1 minute then try login to your server through SSH again and continue the next step.