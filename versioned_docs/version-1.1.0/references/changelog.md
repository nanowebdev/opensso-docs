---
sidebar_position: 1
---

# Changelog

### v1.1.0 - 08/2023

- [x] Add Oauth for Apple ID.
- [x] Update email for reset password.
- [x] Update config.
- [x] Update dependencies.
- [x] Update postman file.
- [x] Update docs about API.
- [x] Add docs how to upgrade.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **config.js** - Add new configuration for oauth apple.
:::
---

### v1.0.0 - 03/2023

- [x] First Release.
