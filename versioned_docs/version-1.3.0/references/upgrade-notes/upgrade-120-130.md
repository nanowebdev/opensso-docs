---
sidebar_position: 3
---

# Upgrade 1.2.0 - 1.3.0
In this version, there is break changes happened because OpenSSO will not support NodeJS 14 anymore and there is some files has changed. Just replace it will be more easier, but if you have modified OpenSSO code by yourself, then you should consider about these files:

```bash title="File Changes"
root
├── views
│   └── default
│       ├── sign-in.html
│       └── backend
│               └── _global_modal.html
├── views
│   └── inverse
│       ├── sign-in.html
│       └── backend
│               └── _global_modal.html
├── routes
│   └── user.js
├── .gitlab-ci.yml
├── changelog.md
├── package-lock.json
├── package.json
├── readme.md
.
```

## How to manually Upgrade ?  
Please attention, in this upgrade, there is a break changes on dependencies, which is OpenSSO is no longer support for NodeJS v14. So you need to be more carefully to follow this upgrade steps.

:::danger Alwways Backup
**Please Remember to always backup before doing any upgrades.**
:::

:::tip Try on local
To increase the successful possibilty when doing upgrading, always try on your local computer first.
:::

### 1. Replace the old `package-lock.json` and `package.json`.
Just replace it with newer file from this version.

### 2. Delete current `node_modules` directory.

```bash title="Then run this command"
npm install
```
It will automatically installing the new package and dependencies.  

### 3. Done
Now you can try to run the OpenSSO application.

```bash
node server.js
```

or

```bash
npm start
```

