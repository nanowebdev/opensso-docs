---
sidebar_position: 1
---

# Deploy to cPanel
cPanel is the most popular control panel in shared hosting. Please make sure your cPanel already support NodeJS.

:::info Database

We assume that you already setup the database correctly. If not, please refer to this docs **[Restore Database](/docs/get-started#b-restore-database-optional-you-can-skip-this-part)**.

:::

:::tip Using default database with SQLite

You can skip to use database engine, like MySQL, MariaDB, PostgreSQL, etc.  
Just using default database SQLite, it's enough for small website.

:::

### Preparation
1. Edit the `config.js` file, then change this part with your domain or subdomain, in this case I'll using my subdomain `sso.sail-it.id`.  
```js title="config.js"
 // WEBSITE
  // This is the base url, used for template engine
  // Change with your domain without trailing slash.
  // Example : https://yourdomain.com
  // highlight-next-line
  baseUrl: 'https://sso.sail-it.id',
  // highlight-next-line
  baseAssetsUrl: 'https://sso.sail-it.id',
  siteName: 'OpenSSO',
```
2. Then save it.
3. Now select all files then compressed to zip, give it name `app.zip`.
4. Done.

:::tip node_modules
You are able to install node_modules locally then zip all and upload it to your cPanel.  
But npm install from NodeJS App cPanel are recommended.
:::

### Upload to hosting
1. Go to your `cPanel` and look for `File Manager`.
2. Create new folder name `app` on root.  
![](/img/content/tutorial/cpanel-1.png)  
3. Go inside app folder, then click `Upload` button at top left.  
Then you will being redirected to the new page for upload, now choose your zip file and start upload it.  
![](/img/content/tutorial/cpanel-2.png)  
When the upload complete, you can close it page and back to `File Manager`.
4. Click `Reload` button then you will see your `app.zip` file on there.  
Now right click on that file, then choose `Extract`.    
![](/img/content/tutorial/cpanel-3.png)  
Just type `/app` on it, then click `Extract Files`.  
![](/img/content/tutorial/cpanel-4.png)  
When the extract successfuly, it will shown like this below  
![](/img/content/tutorial/cpanel-5.png)  
Just click `Close` and click `Reload` button again. Then you will see app files already extracted.
![](/img/content/tutorial/cpanel-6.png)
5. Done, you can close the `File Manager` page.

### Setup NodeJS App
1. Go to your `cPanel` again.
2. Look for `Setup NodeJS App` then click `CREATE APPLICATION` button.  
![](/img/content/tutorial/cpanel-7.png)
3. Configuration should look like this below, then click `CREATE` button
![](/img/content/tutorial/cpanel-8.png)
4. Then click `Start App`, if there is no error, the button will display like this below
![](/img/content/tutorial/cpanel-9.png)
5. Done, now try to visit the website.  
![](/img/content/tutorial/cpanel-11.png)

:::danger Troubleshoot
If you found a blank white screen, then it might be:
- Some of hosting provider is having issue with SQLite support for NodeJS.
- or might be the library was broken or permissions issue.

The solution is:  
1. You have to choose other database except `SQLite`, i.e `MySQL` or `PostgreSQL`.
2. Remove library sqlite3 from package by running `npm remove sqlite3`.  
3. More details configuration, please read here >> **[Configuration Database](/docs/guides/configuration/database)**.
:::

### Create Admin User
If everything is running well, then let's create a first user as Admin.
1. Just go to `Sign Up` page.
2. Fill the form and click `Sign Up` button.  
![](/img/content/tutorial/cpanel-12.png)
3. Done, the first user will get admin user automatically.

