---
sidebar_position: 1
---

# Changelog

### v1.5.0 - 06/24

- [x] Create new API for handling import user.
- [x] Disallow register API for handling import user.
- [x] Add more security layer for using SSO.
- [x] Add webhook for broadcasting/event streaming support.
- [x] Add new API to generate new SSO key.
- [x] Updating data SSO should not auto generating new SSO key.
- [x] Fixed apple signin issue (user didn't knew if popup blocked).
- [x] Fixed method and service is null when adding a new user by admin.
- [x] Fixed dynamic user data should be wrapped to an object type.
- [x] Fixed some minor bugs on ui.
- [x] Update database.
- [x] Update postman file.
- [x] Update online documentation.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **A lot of modification on Default & Inverse Theme**.
- **Database structure changed** - Added new 3 fields on `data_ssos` table and new table `log_webhooks`.
- **config.js** - Added new configuration trustProxy.
- **server.js** - Added new routes for log webhook.
:::
---

### v1.4.0 - 06/2024

- [x] Now support 2FA/MFA via Email and/or Authenticator.
- [x] Now support use a custom id and/with hash value when registering new user.
- [x] Adding new optional reply_to parameter on Sendmail API.
- [x] Simplify email content to avoid spam trigerred issue.
- [x] Removed make-promises-safe, it's already fixed by official nodejs dev since node v16.
- [x] Fixed can't reply directly to the incoming mail from contact.
- [x] Update database.
- [x] Update postman file.
- [x] Upgrade Fastify to 4.27.x.
- [x] Upgrade dependencies.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **A lot of modification on Default & Inverse Theme**.
- **Database structure changed** - Added new 4 fields on users table.
- **server.js** - Removed make-promises-safe.
- **config.js** - Added new configuration but backwards support.
- **package.json** - Added new otpauth library.
:::
---

### v1.3.0 - 01/2024

- [x] Now support adding custom field on user data when registering new user via API.
- [x] Fixed updating user will not always replacing old custom data field.
- [x] Fixed Apple Sign-In not working on login page.
- [x] Fixed global modal for change password, sometimes could injected by browser after login.
- [x] Upgrade Fastify to 4.25.x.
- [x] Update PDF Documentation.
- [x] Upgrade dependencies.
- [x] No Longer support NodeJS 14.
---

### v1.2.0 - 10/2023

- [x] Add switcher light or dark theme.
- [x] Add new inverse theme.
- [x] Now Support Docker and Docker Compose.
- [x] Extend username length up to 64 chars, [RFC 5321](https://www.rfc-editor.org/rfc/rfc5321#section-4.5.3.1.1).
- [x] Fixed date format on export.
- [x] Removed Redis and base-64 dependencies.
- [x] Add Email Logo link from configuration.
- [x] Upgrade dependencies.
- [x] Update pdf documentation.
- [x] Update configuration.
- [x] Update server.js.
- [x] Update template.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **Default Theme will not working**.
- **server.js** - Removed Redis Package, Update Fastify Cacheman.
- **config.js** - Added new configuration for logo on email.
:::
---

### v1.1.0 - 08/2023

- [x] Add Oauth for Apple ID.
- [x] Update email for reset password.
- [x] Update config.
- [x] Update dependencies.
- [x] Update postman file.
- [x] Update docs about API.
- [x] Add docs how to upgrade.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **config.js** - Add new configuration for oauth apple.
:::
---

### v1.0.0 - 03/2023

- [x] First Release.
