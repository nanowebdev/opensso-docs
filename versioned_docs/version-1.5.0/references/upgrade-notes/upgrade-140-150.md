---
sidebar_position: 5
---

# Upgrade 1.4.0 - 1.5.0
In this version, there is break changes happened because OpenSSO now support webhook for broadcasting/event streaming which is many things are changed and there is some new files added. **Just replace it, will be more easier**, but if you have already modified OpenSSO code by yourself, then you should consider about these files:

```bash title="File New"
root
├── models/
│   └── log_webhook.js
├── routes/
│   └── log_webhook.js
├── schemas/
│   └── log_webhook.js
├── views/
│   └── default/
│       └── backend/
│           ├── data_sso_log_webhook.html
│           └── my_sso_log_webhook.html
├── views/
│   └── inverse/
│       └── backend/
│           ├── data_sso_log_webhook.html
│           └── my_sso_log_webhook.html
.
```

```bash title="File Changes"
root
├── database/
│   ├── data_default.sqlite3
│   ├── data.sqlite3
│   ├── db.mysql.sql
│   ├── db.postgre.sql
│   └── db.sql
├── lib/
│   └── helper.js
├── models/
│   └── data_sso.js
├── plugins/
│   └── fastify-reply-handler.js
├── public/
│   └── assets/
│       └── default/
│           └── js/
│               ├── argon-dashboard.js
│               └── argon-dashboard.min.js
├── public/
│   └── assets/
│       └── inverse/
│           └── js/
│               ├── argon-dashboard.js
│               └── argon-dashboard.min.js
├── routes/
│   ├── data_sso.js
│   ├── oauth.js
│   ├── page-internal.js
│   ├── page.js
│   └── user.js
├── schemas/
│   ├── data_sso.js
│   └── user.js
├── test/
│   └── helper.js
├── views/
│   └── default/
│       ├── backend/
│       │   ├── _global_js_.html
│       │   ├── data_sso.html
│       │   ├── data_user.html
│       │   ├── example_data.html
│       │   └── my_sso.html
│       ├── _global_js.html
│       ├── contact.html
│       ├── forgot-password.html
│       ├── sign-in.html
│       ├── sign-up.html
│       └── sso-login.html
├── views/
│   └── inverse/
│       ├── backend/
│       │   ├── _global_js_.html
│       │   ├── data_sso.html
│       │   ├── data_user.html
│       │   ├── example_data.html
│       │   └── my_sso.html
│       ├── _global_js.html
│       ├── contact.html
│       ├── forgot-password.html
│       ├── sign-in.html
│       ├── sign-up.html
│       └── sso-login.html
├── changelog.md
├── config.default.js
├── config.js
├── docker-compose.yml
├── package-lock.json
├── package.json
├── postman_collection.json
├── server.js
.
```

## How to manually Upgrade ?  
Please attention, in this upgrade, there is a lot of break changes. So you need to be more carefully to follow this upgrade steps.

:::danger Alwways Backup
**Please Remember to always backup before doing any upgrades.**
:::

:::tip Try on local
To increase the successful possibilty when doing upgrading, always try on your local computer first.
:::

### 1. Update database structure
You have to execute this SQL query below, only once. Choose what database you're using.

```sql title="MySQL"
ALTER TABLE data_ssos ADD COLUMN mode_strict INT DEFAULT 0;
ALTER TABLE data_ssos ADD COLUMN whitelist TEXT;
ALTER TABLE data_ssos ADD COLUMN webhook_url TEXT;

DROP TABLE IF EXISTS `log_webhooks`;
CREATE TABLE `log_webhooks` (
	`event_id` varchar(100) NOT NULL,
	`event_key` varchar(100) NOT NULL,
	`event_type` varchar(100) DEFAULT NULL,
	`event_name` varchar(100) DEFAULT NULL,
	`event_owner` varchar(64) DEFAULT NULL,
	`event_data` text,
  	`created_at` bigint(13) DEFAULT NULL,
	`created_at_month` varchar(100) DEFAULT NULL,
  	`created_at_year` int(11) DEFAULT NULL,
	`endpoint` text,
	"http_status" int(11) DEFAULT NULL,
	"http_response"	text,
  PRIMARY KEY (`event_id`),
  KEY `index_log_webhooks` (`event_key`,`event_owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

```sql title="PostgreSQL"
ALTER TABLE data_ssos ADD COLUMN mode_strict INT DEFAULT 0;
ALTER TABLE data_ssos ADD COLUMN whitelist TEXT DEFAULT '[]';
ALTER TABLE data_ssos ADD COLUMN webhook_url TEXT DEFAULT '';

DROP TABLE IF EXISTS log_webhooks;
CREATE TABLE log_webhooks (
	event_id varchar(100) NOT NULL,
	event_key varchar(100) NOT NULL,
	event_type varchar(100) DEFAULT NULL,
	event_name varchar(100) DEFAULT NULL,
	event_owner varchar(64) DEFAULT NULL,
	event_data text,
	created_at bigint DEFAULT NULL,
	created_at_month varchar(100) DEFAULT NULL,
  created_at_year int DEFAULT NULL,
	endpoint text,
	http_status int DEFAULT NULL,
	http_response	text,
  PRIMARY KEY (event_id)
);

CREATE INDEX index_log_webhooks ON log_webhooks (event_key, event_owner);
```

```sql title="SQLite3"
ALTER TABLE data_ssos ADD COLUMN mode_strict INTEGER DEFAULT 0;
ALTER TABLE data_ssos ADD COLUMN whitelist TEXT DEFAULT '[]';
ALTER TABLE data_ssos ADD COLUMN webhook_url TEXT DEFAULT '';

CREATE TABLE IF NOT EXISTS "log_webhooks" (
	"event_id"			TEXT,
	"event_key"			TEXT,
	"event_type"		TEXT,
	"event_name"		TEXT,
	"event_owner"		TEXT,
	"event_data"		TEXT,
	"created_at"		INTEGER,
	"created_at_month"	TEXT,
  "created_at_year"	INTEGER,
	"endpoint"			TEXT,
	"http_status"		INTEGER,
	"http_response"		TEXT
);
CREATE INDEX IF NOT EXISTS "index_log_webhooks" ON "log_webhooks" (
	"event_key",
	"event_owner"
);
```

### 2. Modify your `config.js`

Add `trustProxy` configuration at line 8 of your `config.js`.  
Its located after `logger`.

```js title="config.js"
{
  // ...
  logger: true, // Server Log (default is true)
  // highlight-next-line
  trustProxy: false, // Server will trust proxy (default is false)
  // ...
}

module.exports = config
```

### 3. Modify file `server.js`

```js title="File server.js"
// ...
const config = require('./config.js')
const cluster = require('cluster')
// highlight-next-line
const helper = require('./lib/helper.js')
const numCPUs = require('os').cpus().length
const htmlMinifier = require('html-minifier-terser')
const server = require('fastify')({
  logger: config.logger,
  // highlight-next-line
  trustProxy: helper.getConfig('trustProxy', false),
  maxParamLength: config.maxParamLength
})
// ...

// Cors
server.register(require('@fastify/cors'), {
  origin: '*',
  methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS',
  // highlight-next-line
  allowedHeaders: 'Content-Type, Authorization, X-Requested-With, Etag, X-Token, X-Email-OTP, X-GA-OTP, Access-Key, Access-Token'
})

//...
```
- Add `const helper = require('./lib/helper.js')`
- Add `trustProxy: helper.getConfig('trustProxy', false),`
- Add `Access-Key` and `Access-Token`.
- Then save it.

### 4. Replace the old `package-lock.json` and `package.json`.
Just replace it with newer file from this version.

### 5. Delete current `node_modules` directory.

```bash title="Then run this command"
npm install
```
It will automatically installing the new package and dependencies.  

### 6. Done
Now you can try to run the OpenSSO application.

```bash
node server.js
```

or

```bash
npm start
```

