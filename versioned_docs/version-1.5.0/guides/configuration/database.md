---
sidebar_position: 2
---

# Database
OpenSSO support multiple databases. The default is using SQLite3.  

Here is the example database configuration:  
### **a. SQLite3**  
```js
sequelizeOption: {
  dialect: 'sqlite',
  storage: './database/data.sqlite3'
},
```

### **b. RDBMS**  
```js
sequelizeOption: {
  dialect: 'mariadb', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  dbname: 'DATABASE_NAME',
  username: 'DATABASE_USER_NAME',
  password: 'DATABASE_USER_PASSWORD',
  options: {
    host: 'DATABASE_HOST_OR_SERVER',
    port: 'DATABASE_PORT'
  },
//   dialect: 'sqlite',
//   storage: './database/data.sqlite3'
},
```

:::secondary
- If you are using RDBMS, you have to **[Restore Database](/docs/get-started#b-restore-database-optional-you-can-skip-this-part)**.
- **dialect** is the name of database engine, choose one of `mysql`, `mariadb`, `postgres` or `mssql`.
:::
