---
sidebar_position: 6
---

# Mailer
OpenSSO using mailer for activate user, send message contact, forgot password, etc. So you have to activate this.

```js title="config.js"
useMailer: true,
recipientEmailAddress: 'your_inbox_mail@gmail.com',
nodeMailerTransport: {
  pool: true,
  host: 'smtp.zoho.com',
  port: 465,
  secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'your_noreply_mail@zohomail.com',
    pass: 'xxxxxxx'
  }
},
```

**Description:**
- **useMailer** is to activate the mailer system.
- **recipientEmailAddress** message from contact us will be delivered to here.
- **nodeMailerTransport** is the configuration from nodemailer.

:::secondary
- I recommend to use [Zoho Mail](https://zoho.com), because it's very easy and cheap.
- If you want to use free smtp service, [Elastic Email](https://elasticemail.com) is a good option for you.
- If you need custom or more advanced configuration, please see >> [https://nodemailer.com](https://nodemailer.com).
:::

:::tip Activation User
You can prevent BOT / Spam on user registration by sending an activation link via email.

```js title="config.js"
// ---
useActivationUser: true,
// ---
```
:::
