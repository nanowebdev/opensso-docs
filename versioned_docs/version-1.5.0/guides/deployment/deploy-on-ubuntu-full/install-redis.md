---
sidebar_position: 4
---

# IV. Install Redis
Redis is an open-source in-memory data structure store. It can serve as a database cache and message broker and works well with web applications.

## 1. Setup Repository
We should setup repository for Redis because the default Redis version from ubuntu is very old. We should update it, so we can use the latest Redis version.

:::tip
Beginning with version 6, Redis maintains multi-user security through an Access Control List (ACL).  
So in this guide, we going to use latest Redis 6 or higher.
:::

### Prerequest Install
```bash
apt install software-properties-common
```

### a. Add Redis Repository
```bash
add-apt-repository ppa:redislabs/redis
```

### b. Update Packages
```bash
 apt update
```

## 2. Install Redis

### a. Installing
```bash
apt install redis-server -y
```

### b. Check Version
```bash
redis-cli --version
```

```bash title="output"
redis-cli 7.2.1
```

:::tip GOOD
It is good if your redis is higher than version 6.
:::

## 3. Enable and Run Redis

### a. Change to use Systemd
Edit the `redis.conf`.
```bash
nano /etc/redis/redis.conf
```

Change the value of the `supervised` directive to `systemd`
```bash title="Opened file: /etc/redis/redis.conf"
# ---
supervised systemd
# ---
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### b. Allow Remote Connection
Edit the `redis.conf`.
```bash
nano /etc/redis/redis.conf
```

Change the value of the `bind 127.0.0.1 -::1`
```bash title="Opened file: /etc/redis/redis.conf"
# ---
bind 0.0.0.0 -::1
# ---
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### c. Restart Redis
```bash
systemctl restart redis.service
```

If you found it error redis.service not found,  
try to restart on different way
```bash
systemctl restart redis-server
```

### d. Enter the interactive Redis CLI.
```bash
redis-cli
```

try to `ping`
```bash
PING
```

If Redis is running, it returns a `PONG` as a reply.
```bash title="output"
PONG
```

Now exit from CLI
```bash
exit
```

## 4. Secure Redis

### a. Create Default Password
Default new installed redis, is no using password, so we should protect our redis by set the default password.

Change the `requirepass` variable in the `redis.conf` file to set the default password.

```bash
nano /etc/redis/redis.conf
```

Try searching `requirepass foobared`, then replace it with yourpassword

```bash title="Opened file: /etc/redis/redis.conf"
# ---
requirepass yourpassword
# ---
```

Restart Redis to force the changes to take effect.
```bash
systemctl restart redis.service
```

If you found it error redis.service not found,  
try to restart on different way
```bash
systemctl restart redis-server
```

### b. Try using ACL
:::tip INFO
ACL feature is available since Redis 6.
:::

Try to test ACL, let's enter the CLI
```bash
redis-cli
```

Try typing
```bash
ACL LIST
```

If you see,
```bash title="output"
127.0.0.1:6379> (error) ERR unknown command 'ACL'
```
Then it means you are using older than Redis 6.

:::danger Old Redis
You have to upgrade your Redis to 6 or higher.  
You can not continue, if stopped at here.
:::

If you see,
```bash title="output"
1) "user default on nopass sanitize-payload ~* &* +@all"
```
Then it means you didn't set a default password for redis.


If you already set the default password, it must be like this
```bash title="output"
(error) NOAUTH Authentication required.
```

Then you can try typing
```bash
exit
```

Then you can try to come inside with your default password
```bash
redis-cli -a yourpassword
```

Then you can try typing again
```bash
ACL LIST
```

So it would return like this
```bash title="output"
1) "user default on sanitize-payload #816fd38b9c73259d4445dd41ef24670d627e7166a0c489d48e4f33241529d67b ~* &* +@all"
```

### c. Create user ACL
Edit the `redis.conf`
```bash
nano /etc/redis/redis.conf
```

Add this list user on very last line
```bash title="Opened file: /etc/redis/redis.conf"
user youruser2 +@all allkeys on >user2pass
user youruser3 +@all -SET allkeys on >user3pass
```
:::tip 
The difference `youruser2` and `youruser3` is,
- `youruser2` is able to use all redis commands
- `youruser3` is able to use all redis commands except SET command.
:::

Then save it by press `ctrl+x` then press `y` and `enter`.

Restart Redis to force the changes to take effect.
```bash
systemctl restart redis.service
```

If you found it error redis.service not found,  
try to restart on different way
```bash
systemctl restart redis-server
```

### d. Test Auth with default password

Try to enter CLI
```bash
redis-cli
```

Input default password
```bash
AUTH yourpassword
```

Check ACL list
```bash
ACL LIST
```

It should return like this
```bash title="output"
1) "user default on sanitize-payload #816fd38b9c73259d4445dd41ef24670d627e7166a0c489d48e4f33241529d67b ~* &* +@all"
2) "user youruser2 on sanitize-payload #8fbe8e13a025185501f49e0fbefc071711b2b78dfc3f2e31dd75747e19fa0f42 ~* resetchannels +@all"
3) "user youruser3 on sanitize-payload #d48b9943c97a1cc654ec36b968d1158a9be20adcb2a8622b0289587ff09fee8f ~* resetchannels +@all -set"
```

Now exit
```bash
exit
```

### e. Test with user ACL password

Enter CLI again
```bash
redis-cli
```

Then try to login with new user
```bash
AUTH youruser2 user2pass
```

It should return OK
```bash title="output"
OK
```

:::tip Congratulations
Now your redis already secured.
:::

## 5. Optimize Redis

### 1. Set overcommit and maxcon
Edit the `sysctl.conf`

```bash
nano /etc/sysctl.conf
```

Put it on the very last line
```bash title="Opened file: /etc/sysctl.conf"
# ---
vm.overcommit_memory = 1
net.core.somaxconn = 1024
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### 2. Set Maximum Memory
maxmemory is the correct configuration option to prevent Redis from using too much RAM.

Edit the `redis.conf`

```bash
nano /etc/redis/redis.conf
```
Try searching `maxmemory <bytes>` then replace it with
```bash title="Opened file: /etc/redis/redis.conf"
maxmemory 1073741824
```

Then save it by press `ctrl+x` then press `y` and `enter`.

:::secondary
- maxmemory is calculated in bytes.
- 1073741824 bytes is around 1Gb of RAM.
- 2147483648 bytes is around 2Gb of RAM.
- You could increase it whatever as you like.
:::

### 3. Reboot Server Machine
Now reboot the server to take the effect.
```bash
reboot
``` 


## 6. Completely Uninstall
If you has been failed to configure the Redis or just in case want to completely uninstall Redis.

Uninstall
```bash
apt-get purge --auto-remove redis-server
```

Now Reboot the server to take effect
```
reboot
```
