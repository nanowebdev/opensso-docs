---
sidebar_position: 6
---

# VI. Install Docker
Docker Engine is an open source containerization technology for building and containerizing your applications.

## 1. Avoid any conflict packages
Before you can install Docker Engine, you must first make sure that any conflicting packages are uninstalled.

Distro maintainers provide an unofficial distributions of Docker packages in APT. You must uninstall these packages before you can install the official version of Docker Engine.

Run the following command to uninstall all conflicting packages:
```bash
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
```
`apt-get` might report that you have none of these packages installed.

Images, containers, volumes, and networks stored in `/var/lib/docker/` aren't automatically removed when you uninstall Docker. If you want to start with a clean installation, and prefer to clean up any existing data, read the [Uninstall Completely](#5-uninstall-completely) section.

## 2. Setup Repository
Before you install Docker Engine for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

### a. Update packages
```bash
apt update
```

### b. Add repository
```bash
# Add Docker's official GPG key:
apt install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### c. Update Package
We already add new official docker repository, so we must reupdate package again.
```bash
apt update
```

## 3. Install Docker
Now we ready to install Docker.
```bash
apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

## 4. Verify Docker
Verify that the Docker Engine installation is successful by running the hello-world image.
```bash
docker run hello-world
```

## 5. Manage Log
Docker doesn't handle the log as default, so the log size will going higher everyday.
To handle this, we have to create the logrotate configuration to manage the log.
```bash
sudo nano /etc/logrotate.d/docker-containers
```

then paste this
```bash
/var/lib/docker/containers/*/*.log {
    rotate 5
    daily
    compress
    missingok
    notifempty
    create 640 root adm
}
```
Then save it by press `ctrl+x` then press `y` and `enter`.

This configuration will rotate Docker container logs daily, keep 5 rotations, and compress old logs.

## 6. Uninstall Completely
If you has been failed to configure the Docker or just in case want to completely uninstall Docker.

### 1. Uninstall all Docker packages
Uninstall the Docker Engine, CLI, containerd, and Docker Compose packages:
```bash
apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
```

### 2. Delete files
Images, containers, volumes, or custom configuration files on your host aren't automatically removed. To delete all images, containers, and volumes:
```bash
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
```
