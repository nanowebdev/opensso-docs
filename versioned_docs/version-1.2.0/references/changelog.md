---
sidebar_position: 1
---

# Changelog

### v1.2.0 - 10/2023

- [x] Add switcher light or dark theme.
- [x] Add new inverse theme.
- [x] Now Support Docker and Docker Compose.
- [x] Extend username length up to 64 chars, [RFC 5321](https://www.rfc-editor.org/rfc/rfc5321#section-4.5.3.1.1).
- [x] Fixed date format on export.
- [x] Removed Redis and base-64 dependencies.
- [x] Add Email Logo link from configuration.
- [x] Upgrade dependencies.
- [x] Update pdf documentation.
- [x] Update configuration.
- [x] Update server.js.
- [x] Update template.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **Default Theme will not working**.
- **server.js** - Removed Redis Package, Update Fastify Cacheman.
- **config.js** - Added new configuration for logo on email.
:::
---

### v1.1.0 - 08/2023

- [x] Add Oauth for Apple ID.
- [x] Update email for reset password.
- [x] Update config.
- [x] Update dependencies.
- [x] Update postman file.
- [x] Update docs about API.
- [x] Add docs how to upgrade.

:::danger Break Changes
Please be careful, this version has a Break Changes on :
- **config.js** - Add new configuration for oauth apple.
:::
---

### v1.0.0 - 03/2023

- [x] First Release.
