---
sidebar_position: 1
---

# Deploy OpenSSO Native
This guide will explain how to deploy and run OpenSSO in native way.

:::info
Before follow this guide, make sure on your Ubuntu server:
- A PostgreSQL has been installed.
- A file `opensso/database/db.postgre.sql` has been restored to your PostgreSQL database.
:::

:::danger disclaimer
Please try with fresh installed Ubuntu 20,  
because I don't responsible with any data loss on your server.
:::

## 1. Install NodeJS
This will install NodeJS 16.x.x.
```bash
apt install gcc g++ make curl -y
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
apt install nodejs -y
```

:::tip
I recommend to use NodeJS 18 instead of NodeJS 16.
:::

## 2. Upload OpenSSO
When you buy the OpenSSO from CodeCanyon, you got it as a zipped file named `open-sso-1.2.0.zip`.
You can use FileZilla, WinSCP, etc. to upload `open-sso-1.2.0.zip` to your Ubuntu server.

:::secondary
If you follow the **[Get Started guide](/docs/get-started)**,  
then you have to zip it back and upload it to your Ubuntu server.
:::

### a. Extract it
We will need to install unzip.
```bash
apt install unzip -y
```

First, you should create directory `opensso`.
```bash
mkdir /srv/opensso/
```
Now you can extract `open-sso-1.2.0.zip` to `/srv/opensso`.

Make sure you still in the same directory of `open-sso-1.2.0.zip`.  
Then extract it to `/srv/opensso/`.
```bash
unzip open-sso-1.2.0.zip -d /srv/opensso
```

So now the directory `/srv/` will be look like this.

```bash
├── srv
│   └── opensso
```

and the inside directory `opensso` should be look like this.
```bash
├── srv/opensso
│   ├── changelog.md
│   ├── config.default.js
│   ├── config.js
│   ├── database
.
```

## 3. Configure OpenSSO

Go to directory `/srv/opensso`.
```bash
cd /srv/opensso
```

### a. Setup connection database
Edit the `config.js`
```bash
nano config.js
```

```bash title="Opened file: config.js"
const config = {
  // ---
  sequelizeOption: {
    dialect: 'postgres',
    dbname: 'postgres',
    username: 'postgres',
    password: 'yourpassword',
    options: {
      host: 'localhost',
      port: '5432'
    }
    // dialect: 'sqlite',
    // storage: './database/data.sqlite3'
  },
  // ---
}
```

Then save it by press `ctrl+x` then press `y` and `enter`.

:::secondary
Above is a standard example configuration of using PostgreSQL database.
:::

### b. Set Sub Domain for Website
Edit the `config.js`
```bash
nano config.js
```

```bash title="Opened file: config.js"
const config = {
  // ---
  host: '0.0.0.0',
  port: 3000,
  // ---
  baseUrl: 'http://sso.yourdomain.com',
  baseAssetsUrl: 'http://sso.yourdomain.com',
  // ---
}
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### c. Build App
Make sure you're still on directory `/srv/opensso/`.

Try to build app again.
```bash
npm install
```

## 4. Setup OpenSSO Service
We have to create a service for OpenSSO in systemd.

### a. Create a service opensso
```bash
nano /etc/systemd/system/opensso.service
```

Then paste this
```bash title="Opened file: /etc/systemd/system/opensso.service"
[Unit]
Description=OpenSSO
Documentation=http://google.com
After=network.target
[Service]
Environment=NODE_PORT=3000
Type=simple
User=root
Group=root
# Output to syslog
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=opensso
ExecStart=/usr/bin/node /srv/opensso/server.js
WorkingDirectory=/srv/opensso
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### b. Start OpenSSO Service
```bash
systemctl start opensso
```

### c. Make it Autostart
```bash
systemctl enable opensso
```

## 5. Setup Nginx
If you already install Nginx, you can skip this.

### a. Install Nginx
```bash
apt install nginx -y
```

Now check nginx status
```bash
systemctl status nginx
```

Then it will show like this
```bash title="output"
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2023-04-20 16:08:19 UTC; 3 days ago
     Docs: man:nginx(8)
 Main PID: 2369 (nginx)
    Tasks: 2 (limit: 1153)
   Memory: 3.5M
   CGroup: /system.slice/nginx.service
           ├─2369 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
           └─2380 nginx: worker process
```

### b. Adjust Firewall
Ubuntu 20 already install UFW as default.  
You have to activate it.
```bash
ufw enable
```

Now we would allow `Nginx HTTP` means firewall will open NGINX for port 80 only.
```bash
ufw allow 'Nginx HTTP'
```

You can verify it.
```bash
ufw status
```

Now make sure public could access it.
```bash
curl -4 icanhazip.com
```

So it will return your public ip address
```bash title="output"
123.123.123.123
```

:::secondary
The result output must be different with yours.  
123.123.123.123 is just an example of pubic ip address.
:::

### c. Configure server block
Edit the `nginx.conf`
```bash
nano /etc/nginx/nginx.conf
```

Just add comment for this line
```bash
#include /etc/nginx/sites-enabled/*;
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### d. Create domain.conf
Now create the `domain.conf` to `/etc/nginx/conf.d/domain.conf`.

```bash
nano /etc/nginx/conf.d/domain.conf
```

Then paste this
```bash title="Opened file: /etc/nginx/conf.d/domain.conf"
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name sso.yourdomain.com;

    root /srv/opensso/public;

    location / {
        proxy_pass              http://127.0.0.1:3000;
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;
    }

    # cache all files inside root with expires max
    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|ico|zip|tgz|gz|rar|ico|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|rtf|txt)$ {
        access_log off;
        log_not_found off;
        expires max;
        add_header Pragma public;
        add_header Cache-control "public";
    }
}
```

Then save it by press `ctrl+x` then press `y` and `enter`.

### e. Make a Test
Make sure your `domain.conf` is correct.
```bash
nginx -t
```

```bash title="output"
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

### f. Restart Nginx
When nginx configuration is correct,
then you can just restart the Nginx service.
```bash
systemctl restart nginx
```

:::tip Congratulations
Now you can access OpenSSO at http://sso.yourdomain.com.  

If you can't reach it,  
Make sure your domain DNS already pointing to your Ubuntu server.
:::
