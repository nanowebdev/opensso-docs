---
sidebar_position: 4
---

# Integration
This guide will explain how to integrate OpenSSO to your new project or existing application which is already have their own user database.

## Diagram Flow
To integrate OpenSSO, it will require a little programming knowledge.  
For more detail, you can see the flow/diagram about how to integrate OpenSSO. [Click here to see the larger diagram](/img/content/doc-opensso/open-sso-flow.png).  

**Integration OpenSSO to Existing Application Diagram**  

![](/img/content/doc-opensso/open-sso-flow-compressed.png)  



## Integration Steps
Here is the simple steps to integrate OpenSSO.

### A. Get SSO URL Login Page

1. Register an account then Login.
2. Go to menu `My SSO`
3. Add new `SSO`  
![](https://i.imgur.com/XccWQAs.png)
4. Click Submit
5. Done, then you are able to put the `SSO Url` into your website.  
![](https://i.imgur.com/8ZyAUbC.png)
6. Now try to login with our OpenSSO system by visiting the `SSO Url` from your website.

:::secondary
- Field Name is your application or website name.
- Field Callback is the url to redirect user back to your website including the token. 
- When you update, the `Key` and `Callback Url` will always change.
:::

### B. Create Button
Create a button to your website.

```html
<button class="btn btn-sm btn-primary"
  onclick="location.href='https://sso.yourdomain.com/sso/login/xxxxxxxxxx'">
  Single Sign On
</button>
```
The button will redirect user to the SSO URL Login Page. User will required to fill the form login.


### C. Validate JWT Token
Once OpenSSO redirect to your callback page. You have to validate or decode the JWT token from OpenSSO.  

Here is the example function to decoded the JWT token in JavaScript.
```javascript
function parseJWT (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
  return JSON.parse(jsonPayload);
};
```

Here is the example output of decoded JWT token.
```json
{
	"uid": "f1d45f22-e5e9-4077-915e-0278fc578xxx",
	"unm": "admin",
	"name": "admin",
	"mail": "admin@youremail.com",
	"role": "admin",
	"gravatar": "https://gravatar.com/avatar/0e1aa649b4ffff2a7b61e3b7d672b522",
	"hash": "A25NTbjoG1pphZzNjMTI",
	"iat": 1688552848,
	"exp": 1688581648
}
```

**Description**
- `uid` is the user id of user.
- `unm` is the username of user.
- `iat` is identifies the time at which the JWT was issued.
- `exp` is expiration time for JWT.
- `mail` is the email of user.
- `role` is the level of user, only used in OpenSSO and only has two `admin` and `member`.
- `gravatar` is the global avatar of user.
- `hash` is the random generated string, only used for internal system.

:::tip
It's better to validate on server side than on client side. Actualy you can use any libraries to work with JWT Token.
You can choose JWT Libraries that suitable with your application [here](https://jwt.io/libraries).
:::

### D. Check User on Existing Database
If the token successfully validated, then you should check, is the user exist or not?  
If there is no user exists, then you should register and activate it silently with user information from JWT Token to your existing application.


### E. Authenticate / Allow Session
The last steps is you should giving authenticate or allow session to the user. Done.


## Example or Starter Template
We offer the starter template or example scripts that already integrated with OpenSSO into your new static html or new web application.  

:::tip
Using Starter Template will saving many hours just for creating a new application.
:::

**Native:**  
- [Static HTML or Client Side (Pure HTML + JS)](https://gitlab.com/nanowebdev/example/opensso/client-side)
- [Native PHP](https://gitlab.com/nanowebdev/example/opensso/native-php)

**Framework:**  
- [Laravel v8](https://gitlab.com/nanowebdev/example/opensso/laravel-8)
- [TotalJS v4](https://gitlab.com/nanowebdev/example/opensso/totaljs-4)
- [ExpressJS v4](https://gitlab.com/nanowebdev/example/opensso/expressjs-4)
- [FastifyJS v4](https://gitlab.com/nanowebdev/example/opensso/fastify-4)
- [NestJS v10](https://gitlab.com/nanowebdev/example/opensso/nestjs-10)
- [NextJS v13](https://gitlab.com/nanowebdev/example/opensso/nextjs-13)

:::secondary
- BYOB - Bring Your Own Database, the starter template is not included with Database.
- The starter template was created with very minimalist so you are easier to learn.
- I'll update to add more example script in the future.
:::

## F.A.Q
Here is the Frequent Answered Question about Integration OpenSSO.  

### 1. Is this approach safe?
Yes, we follow the JWT standard `Oauth 2.0` as describe in [RFC7519](https://www.rfc-editor.org/rfc/rfc7519) and [RFC9068](https://datatracker.ietf.org/doc/html/rfc9068).  

### 2. Is OpenSSO will work for mobile or any application?
Yes, of course. As long as you can integrate the OpenSSO and manage the JWT token. There are plenty JWT libraries out there. Please see [here](https://jwt.io/libraries).

### 3. Is it easy to Integrate OpenSSO?
To integrate OpenSSO, it will be require a little programming skill.  
I would say it easy if:
- You ever made a application with user management system including login and logout.
- You ever made Login via Google or Login with Facebook.

You can learn how to integrate OpenSSO directly by read our code examples [here](#example-or-starter-template).

If you don't have any programming skill, you should hire the freelancer that have knowledge about NodeJS, JWT and programming language that used in your application.

### 4. Why the roles only admin and member in OpenSSO?
OpenSSO only authenticate user, not giving authorization to the user. Authorization must be applicable on the external application.

### 5. When I logout on my application, how to make my account also logout on OpenSSO?
Actualy there is no way to make user logout on OpenSSO from external application, Because the JWT token is saved on their device or external application, not in the OpenSSO server. This is the benefit of using JWT, because it's stateless.  

For example, there many websites that using facebook login, if you logout from that website, your facebook account still logged in.

Note:
- OpenSSO JWT token will expired automatically in 8 hours as default.

### 6. I have user login system running already, can OpenSSO work as optional user login?
Yes of course, OpenSSO has own user database that different with your existing application.

### 7. I have thousand users, I can't just moving all users for now, is it possible to integrate SSO that would work for old registered user too?
This question is actualy similar like point number 6. But I'll answer more detail.  

This means you will:
- Using two separated user databases.
- Using two login, old login and opensso login.

Then the answer is, yes it's possible. This guide on above is for integrating OpenSSO.

Adding OpenSSO login is just like when you want to adding Google Login or Facebook Login.

Conclusion,
- Your old user still use your existing login.
- Your new user is able to choose existing login or OpenSSO login.

### 8. I have old user login system, can I switch to only use OpenSSO login in the future?
Yes it is possible, please read [Migration](/docs/guides/migration).

### 9. Is it possible to use custom or other Database?
OpenSSO using `Sequelize` libraries that work for multiple mainstream database. It is `SQLite`, `MySQL`, `MariaDB`, `PostgreSQL` and  `MSSQL`. If your custom database not supported by `Sequelize`, so the answer is not possible. You may need to rewrite the OpenSSO source code by your self.

### 10. Is it possible to run OpenSSO on container cloud like docker or other SASS?
Yes it is possible, by default OpenSSO is support to be running inside Docker. About other SASS, might be, You have to configured it by your self.

### 11. OpenSSO error when I try to update the package?
That will happen because you have been update also upgrade some dependencies. If you found an error, you should find what dependencies that trigger the error. I can help you to find out about this. Just feel free to contact me.  

To avoid this, you better to wait until I update the OpenSSO in the future. Application that go for production, should be not too often to update. 

### 12. My question is not here, can I contact you?
Sure, feel free to email me at [aalfiann@gmail.com](mailto:aalfiann@gmail.com).  
Or just chat with me via Telegram [https://t.me/aalfiann](https://t.me/aalfiann).
