---
sidebar_position: 5
---

# Migration
This guide will explain how to migration your existing user into OpenSSO.

:::secondary
- This guide is for completely import all your existing users into OpenSSO.
- Make sure your OpenSSO version is 1.4.0.
:::

## Migration Step

### 1. Make Sure Integration Success
Before doing migration, make sure the integration already successful. Please read about integration [here](/docs/guides/integration).

### 2. Preparation
Before we do importing our users, make sure disable activation user email in `config.js`.

```js title="File config.js"
// ...
    // User Activation by Email
    useActivationUser: false,
//...
```

### 3. Import Existing User
There is two ways to import your existing user.

**A. Import Directly into Database**  
You can use using any Database tools. i.e PHPMyAdmin, SQL Workbench, DBeaver CE, Navicat, etc.

- Export your existing user into csv/xlsx.
- You will required to edit the csv/xlsx following the structure OpenSSO users table.
- Import it using tools.
- Done.

**B. Import using API**  
You can create your own script to import your existing user using API.

|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/user/register | Content-Type: application/json | bodyJson |

bodyJson is a json string:

- If you want to inject a hash password (this way is more safer)
```json
{
	"id": "this id must be same in your existing app",
	"username": "aalfiann",
    "name": "M ABD AZIZ ALFIAN",
	"email": "aalfiann@gmail.com",
	"password": "leave this empty string",
	"hash_value": "this must be same as your hash password in existing app"
}
```

- If you want to just replace it using default password
```json
{
	"id": "this id must be same in your existing app",
	"username": "aalfiann",
    "name": "M ABD AZIZ ALFIAN",
	"email": "aalfiann@gmail.com",
	"password": "1234"
}
```

:::tip
**username** follow [RFC 5321](https://www.rfc-editor.org/rfc/rfc5321#section-4.5.3.1.1).  

It is:
- min length: 3.
- max length: 64.
- lowercase.
- only alphanumeric, dot and underscore is allowed.

Reason:  
Whatever the name part on email will support to be used for username as default for oauth.
:::


:::secondary
- Never tell or announce to anyone the default password, just let them or tell them to use forgot password.
- If your user using fake email, then they will lose their account.
:::

### 4. Activate Email Activation User
You can activate again the `useActivationUser` if you want [Optional].  
Because we already disable it before.

```js title="File config.js"
// ...
    // User Activation by Email
    useActivationUser: true,
// ...
```

### 5. Done
Now all your user already imported into OpenSSO. They will can not using their old password, so just let them using `Forgot Password` to get their account back.