---
sidebar_position: 5
---

# V. Install Nginx
Nginx is available in Ubuntu’s default repositories, it is possible to install it from these repositories using the apt packaging system.

## 1. Install Nginx

### a. Update Packages
```bash
apt update
```

### b. Install Nginx
```bash
apt install nginx -y
```

## 2. Adjusting Firewall
Before testing Nginx, the firewall software needs to be adjusted to allow access to the service.

```bash
ufw app list
```

```bash title="output"
Available applications:
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
  OpenSSH
```

Now we choose `Nginx Full`
```bash
ufw allow 'Nginx Full'
```

Check the status
```bash
ufw status
```

Now your server firewall is allowing Nginx Port.

:::secondary
Nginx Full port is actualy using port 80 and 443
:::

## 3. Check Web Server
Check the nginx service status
```bash
systemctl status nginx
```

```bash title="output"
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2023-04-20 16:08:19 UTC; 3 days ago
     Docs: man:nginx(8)
 Main PID: 2369 (nginx)
    Tasks: 2 (limit: 1153)
   Memory: 3.5M
   CGroup: /system.slice/nginx.service
           ├─2369 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
           └─2380 nginx: worker process
```

Make sure public could access it.
```bash
curl -4 icanhazip.com
```

So it will return your public ip address
```bash title="output"
123.123.123.123
```

When you have your server’s IP address, enter it into your browser’s address bar:
```bash
http://123.123.123.123
```

:::secondary
- IP 123.123.123.123 above is just an example.
- Replace it with your ip public address.
:::

:::tip Congratulations
Now you already have Nginx Server.
:::
