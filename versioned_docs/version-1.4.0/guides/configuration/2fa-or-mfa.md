---
sidebar_position: 12
---

# 2FA/MFA
OpenSSO now support 2FA and/or MFA. This feature was added since **OpenSSO v.1.4.0**. If you want to use this feature, You have to activate this from `config.js`.

```js title="config.js"
{
  // ---
  oauth: {
    // ...
  },

  // TOTP (Time based OTP for 2FA)
  totp: {
    email: {
      // highlight-next-line
      enable: true, // Enabling 2FA via email
      period: 300 // TOTP email period. Default is 300 seconds
    },
    authenticator: {
      // highlight-next-line
      enable: true // Enabling 2FA via authenticator
    }
  }
}

module.exports = config
```

**Description:**
- **email.enable**  if set to true, then this 2FA via email will be activated.
- **email.period**  TOTP email period. Default is 300 seconds but you can adjust it to whatever you need.
- **authenticator.enable**  if set to true, then this 2FA via Authenticator App will be activated.

:::secondary
- If you don't add this totp config to your `config.js`, then 2FA will be disabled as default.
- Don't set `email.period` to be lower than 300 seconds, because sometimes email delivery is very slow.
- TOTP is using time based, the expiration depend on how long the period of time. This means user can use it multiple times until hit expiring time.
- TOTP period for Authenticator is set fixed to 30 seconds, You can't change this.
:::

:::tip
When 2FA is activated, it will give a strong protection to **User Login**.
:::
