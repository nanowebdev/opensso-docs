---
sidebar_position: 8
---

# Template
There is two themes on OpenSSO. It is `default` and `inverse`.

:::info Inverse Theme
We already added new theme named **Inverse** since OpenSSO version 1.2.0.
:::

### How to Change Theme
If you want to change the theme, you have to edit this

```js title="config.js"
// ---
templateDir: 'default',
// ---
```

**Description:**
- **templateDir** is the directory template, it is located inside `views` directory.


### How to Customize Theme
Theme is located at inside `views` directory. To customize it:
1. Just copy the `default` or `inverse` folder.
2. Then paste with different directory name. i.e `my_theme`.
3. Don't forget to change the `config.js`.
```js title="config.js"
// ---
templateDir: 'default',
// ---
```
4. Now you can customize it whatever as you like.

:::tip Customize Theme
We don't recommend to edit the `default` and `inverse` theme directly, except you know exactly what you're doing.  

Because it will make you easier to upgrade OpenSSO in the future, in case if your theme broken, you can simply switch to use `default` or `inverse` theme faster.
:::

### How to Minify
To get the best performance, OpenSSO having HTML cache and also support compression/minifier page.

```js title="config.js"
// ---
isProduction: true,
useHTMLMinifier: true,
minifierOptions: {
  removeComments: true,
  removeCommentsFromCDATA: true,
  collapseWhitespace: true,
  collapseBooleanAttributes: true,
  removeAttributeQuotes: true,
  removeEmptyAttributes: true,
  minifyJS: true,
  minifyCSS: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true,
  removeOptionalTags: true,
  removeRedundantAttributes: true
},
// ---
```

**Description:**
- **isProduction**, if this set to true, then environment will change to production mode which means this also activate the HTML cache.
- **useHTMLMinifier**, if this set to true, HTML source will be minified. Javascript inline also will be minified but not 100%.
- **minifierOptions** is the options for html minifier based on HTML Minifier Terser.

:::secondary
- **minifierOptions** will work if you set **useHTMLMinifier** to true.
- For more detail about **minifierOptions**, please see [HTML Minifier Terser](https://github.com/terser/html-minifier-terser#options-quick-reference).
:::

