---
sidebar_position: 2
---

# Verify Token
If you already having the [X-Token](/docs/guides/api/x-token), then you can access all OpenSSO APIs.
There are two ways to Verify Token from OpenSSO.  

### A. Using JWT Library
Since JWT is stateless, you can verify the Token from OpenSSO using JWT library. Make sure you have the same `public.key`.

This way is very efficient and fast performance, no need any connection to hit your OpenSSO server.

You can find the [JWT libraries here](https://jwt.io/libraries).


### B. Using API
This Verify Token API is used to verify your token. This way is slow performance but good for development phase.

:::tip Verify with JWT
When you have the new X-Token, you shouldn't need to verify it all the time.  
Because JWT already made validation to the token itself based the valid signature and expiry time.
:::


|Method|Endpoint|Header|Body|
|---|---|---|---|
| POST | http://localhost:3000/api/auth/verify | X-Token: {{YOUR_TOKEN}} | |

Example output response
```json
{
    "message": "Verify authentication success!",
    "statusCode": 200,
    "jwt": {
        "uid": "5d37499b-13fc-43fb-9a7b-ddb77757ac93",
        "unm": "aalfiann",
        "name": "aalfiann",
        "mail": "aalfiann@gmail.com",
        "role": "admin",
        "gravatar": "https://gravatar.com/avatar/6d7bd241a2679d755789402681d25ec1",
        "hash": "UxyMTbjoG1pphZzMTMTQ",
        "iat": 1672774870,
        "exp": 1672803670
    }
}
```

**Description**  
- `uid` is your user id.
- `unm` is your username.
- `name` is your fullname.
- `mail` is your email address.
- `role` is the level of user.
- `gravatar` is the global avatar of user.
- `hash` is the random generated string, only used for internal system.

Below here is the JWT standard which is described in [RFC7519](https://www.rfc-editor.org/rfc/rfc7519): 
- `iat` is identifies the time at which the JWT was issued.
- `exp` is expiration time for JWT.


:::secondary
- As alternative, you are able to verify your token through [JWT.io](https://jwt.io).
- To make the signature valid on [JWT.io](https://jwt.io), you have to paste your `private.key` and `public.key` to [JWT.io](https://jwt.io) website.
:::
