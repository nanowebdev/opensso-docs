---
sidebar_position: 4
---

# Response Format
Here is the default response format, any request will having `message` and `statusCode` key properties. The `error` key property will shown only at `4xx` and `5xx` http status code.

- **Success**
```json
{"message": "string", "statusCode": 200}
```
- **Created**
```json
{"message": "string", "statusCode": 201}
```
- **Bad Request**
```json
{"message": "string", "statusCode": 400, "error": "Bad Request"}
```
- **Unauthorized**
```json
{"message": "string", "statusCode": 401, "error": "Unauthorized"}
```
- **Forbidden**
```json
{"message": "string", "statusCode": 403, "error": "Forbidden"}
```
- **Not Found**
```json
{"message": "string", "statusCode": 404, "error": "Not Found"}
```
- **Internal Server Error**
```json
{"message": "string", "statusCode": 500, "error": "Internal Server Error"}
```

