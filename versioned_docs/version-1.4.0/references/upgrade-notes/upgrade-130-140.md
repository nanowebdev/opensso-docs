---
sidebar_position: 4
---

# Upgrade 1.3.0 - 1.4.0
In this version, there is break changes happened because OpenSSO now support 2FA which is many things are changed and there is some new files added. **Just replace it, will be more easier**, but if you have already modified OpenSSO code by yourself, then you should consider about these files:

```bash title="File New"
root
├── lib/
│   └── otp.js
├── public/
│   └── assets/
│       └── vendor/
│           └── qrcodejs/*
├── test/
│   └── otp.js
├── views/
│   └── default/
│       └── email-otp.html
├── views/
│   └── inverse/
│       └── email-otp.html
├── .editorconfig
.
```

```bash title="File Changes"
root
├── database/
│   ├── db.mysql.sql
│   ├── db.postgre.sql
│   └── db.sql
├── lib/
│   ├── helper.js
│   └── password.js
├── models/
│   └── user.js
├── routes/
│   ├── api.js
│   ├── oauth.js
│   └── user.js
├── schemas/
│   ├── mail.js
│   └── user.js
├── test/
│   └── helper.js
├── views/
│   └── default/
│       ├── backend/
│       │   ├── data_user.html
│       │   ├── my_profile.html
│       │   └── support.html
│       ├── contact.html
│       ├── email-activation.html
│       ├── email-reset.html
│       ├── sign-in.html
│       └── sso-login.html
├── views/
│   └── inverse/
│       ├── backend/
│       │   ├── data_user.html
│       │   ├── my_profile.html
│       │   └── support.html
│       ├── contact.html
│       ├── email-activation.html
│       ├── email-reset.html
│       ├── sign-in.html
│       └── sso-login.html
├── changelog.md
├── config.default.js
├── config.js
├── package-lock.json
├── package.json
├── postman_collection.json
├── server.js
.
```

## How to manually Upgrade ?  
Please attention, in this upgrade, there is a lot of break changes. So you need to be more carefully to follow this upgrade steps.

:::danger Alwways Backup
**Please Remember to always backup before doing any upgrades.**
:::

:::tip Try on local
To increase the successful possibilty when doing upgrading, always try on your local computer first.
:::

### 1. Update database structure
You have to execute this SQL query below, only once. Choose what database you're using.

```sql title="MySQL"
ALTER TABLE users ADD COLUMN email_2fa INT DEFAULT 0;
ALTER TABLE users ADD COLUMN ga_2fa INT DEFAULT 0;
ALTER TABLE users ADD COLUMN email_2fa_secret VARCHAR(32) DEFAULT '';
ALTER TABLE users ADD COLUMN ga_2fa_secret VARCHAR(32) DEFAULT '';
```

```sql title="PostgreSQL"
ALTER TABLE users ADD COLUMN email_2fa INT DEFAULT 0;
ALTER TABLE users ADD COLUMN ga_2fa INT DEFAULT 0;
ALTER TABLE users ADD COLUMN email_2fa_secret VARCHAR(32) DEFAULT '';
ALTER TABLE users ADD COLUMN ga_2fa_secret VARCHAR(32) DEFAULT '';
```

```sql title="SQLite3"
ALTER TABLE users ADD COLUMN email_2fa INTEGER DEFAULT 0;
ALTER TABLE users ADD COLUMN ga_2fa INTEGER DEFAULT 0;
ALTER TABLE users ADD COLUMN email_2fa_secret TEXT DEFAULT '';
ALTER TABLE users ADD COLUMN ga_2fa_secret TEXT DEFAULT '';
```

### 2. Modify your `config.js`

Add 2FA configuration at very below of your `config.js`.

```js title="config.js"
{
  // ...
  oauth: {
    // ...
  },

  // TOTP (Time based OTP for 2FA)
  totp: {
    email: {
      enable: false, // Enabling 2FA via email
      period: 300 // TOTP email period. Default is 300 seconds
    },
    authenticator: {
      enable: false // Enabling 2FA via authenticator
    }
  }
}

module.exports = config
```

### 3. Modify file `server.js`

```js title="File server.js"
// ...

// Cors
server.register(require('@fastify/cors'), {
  origin: '*',
  methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS',
  // highlight-next-line
  allowedHeaders: 'Content-Type, Authorization, X-Requested-With, Etag, X-Token, X-Email-OTP, X-GA-OTP'
})

//...
```
Add `X-Email-OTP` and `X-GA-OTP`. Then save it.

### 4. Replace the old `package-lock.json` and `package.json`.
Just replace it with newer file from this version.

### 5. Delete current `node_modules` directory.

```bash title="Then run this command"
npm install
```
It will automatically installing the new package and dependencies.  

### 6. Done
Now you can try to run the OpenSSO application.

```bash
node server.js
```

or

```bash
npm start
```

