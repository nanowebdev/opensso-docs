---
sidebar_position: 1
---

# Upgrade 1.0.0 - 1.1.0
**File Changes**  
This upgrade theres some files has changed. Just replace it but if you have modified OpenSSO code, you should consider about these files:

```bash title="File Changes"
root
├── routes
│   ├── oauth.js
│   └── user.js
├── schemas
│   └── oauth.js
├── views
│   └── default
│       ├── email-reset.html
│       ├── sign-in.html
│       └── sso-login.html
├── changelog.md
├── config.default.js
├── config.js
├── postman_collection.json
├── package-lock.json
├── package-lock.json
.
```


### Add new config.js  
There is new configuration for `oauth`.
```js title="Edit file config.js"
oauth: {
  google: {
    // ...
  },
  apple: {
    enable: false,
    clientId: 'com.xxx.login',
    redirectURI: 'https://yourdomain.com'
  }
}
```


### Upgrade dependencies

1. Uninstall old package
```bash
npm remove @fastify/view eta sqlite3 sequelize fastify
```

2. Install new package
```bash
npm install @fastify/view@7 eta@2 sqlite3 sequelize fastify
```

3. Audit Fix
```bash
npm audit fix
```

4. Done

As you can see, there still 3 package left can't be upgraded because there is still no fix package available, since this is moderate status, we can ignore this for now.

